<?php
declare(strict_types=1);


use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {
  require(dirname(__DIR__).'/config/config.php');
  // Global Settings Object
  $containerBuilder->addDefinitions([
    'settings' => [
      'displayErrorDetails' => $develop_mode, // Should be set to false in production
      'logger' => [
        'name' => 'slim-app',
        'path' => __DIR__ . '/../logs/app.log',
        'level' => Logger::DEBUG,
      ],
      'db' => $db,
      'test_db' => $test_db,
      'page_size' => 20,
      'template' => $template,
      'password' => [
        'user' => [
          'algo' => PASSWORD_BCRYPT,
          'cost' => 9,
        ],
      ],
      'development' => $develop_mode,
      'sql_logger' => [
        'path' => __DIR__ . '/../logs/sql.log',
      ],
      'path' => $path,
      'mail' => $mail,
      'mail_setting' => [
        'from' => '',
        'admin' => '',
      ],
      'encryption' => [
        'password' => 'cnZjU7xvPM8Y',
        'method' => 'AES-128-CBC',
      ],
      'cookie' => [
        'user' => '_user_code',
      ],
      'test_parameter_switch' => false,
      'deploy' => $deploy,
    ],
    'JWT' => [
      'url' => 'http://slim.do.jp',
      'after_time' => 60,
      'expire_time' => 3600,
      'token' => 'F89e3xw63squJB43MHP9brmM4iNy83JAyzVu6n5Zu3QLiDeF',
    ]
  ]);
};
