<?php
declare(strict_types=1);

use App\Application\Middleware\SessionMiddleware;
use App\Application\Middleware\TestMiddleware;
use Slim\App;

return function (App $app) {
    // $app->add(SessionMiddleware::class);
    $app->add(TestMiddleware::class);
};
