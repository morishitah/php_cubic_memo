<?php
declare(strict_types=1);

use App\Application\Dependence\DataDeleteDependence;
use App\Application\Dependence\DataDeleteInterface;
use App\Application\Dependence\DataDetailDependence;
use App\Application\Dependence\DataDetailInterface;
use App\Application\Dependence\DataListDependence;
use App\Application\Dependence\DataListInterface;
use App\Application\Dependence\DataSaveDependence;
use App\Application\Dependence\DataSaveInterface;
use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        DataListInterface::class => function (ContainerInterface $c) {
            $dataList = new DataListDependence();
            $dataList->setContainer($c);
            return $dataList;
        },
        DataDetailInterface::class => function (ContainerInterface $c) {
            $dataList = new DataDetailDependence();
            $dataList->setContainer($c);
            return $dataList;
        },
        DataDeleteInterface::class => function (ContainerInterface $c) {
            $dataList = new DataDeleteDependence();
            $dataList->setContainer($c);
            return $dataList;
        },
        DataSaveInterface::class => function (ContainerInterface $c) {
            $dataList = new DataSaveDependence();
            $dataList->setContainer($c);
            return $dataList;
        },
        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
    ]);
};
