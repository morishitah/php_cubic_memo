<?php
declare(strict_types=1);

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;


return function (App $app) {
  $app->options('/{routes:.*}', function (Request $request, Response $response) {
    // CORS Pre-Flight OPTIONS Request Handler
    return $response;
  });
  $app->GET(API_BASE_URL.'/import', \App\Application\Actions\DataImportAction::class);

  $app->GET(API_BASE_URL.'/login/state', \App\Application\Actions\User\StatusAction::class);
  $app->GET(API_BASE_URL.'/login', \App\Application\Actions\User\LoginCookieAction::class);
  $app->POST(API_BASE_URL.'/login', \App\Application\Actions\User\LoginAction::class);
  $app->GET(API_BASE_URL.'/logout', \App\Application\Actions\User\LogoutAction::class);
  
  $app->group(API_BASE_URL.'/user', function(Group $group) {
    $group->GET('/list[/{page}]', \App\Application\Actions\User\ListAction::class);
    $group->GET('/detail[/{id}]', \App\Application\Actions\User\DetailAction::class);
    $group->POST('/delete', \App\Application\Actions\User\DeleteAction::class);
    $group->POST('/save', \App\Application\Actions\User\SaveAction::class);
    $group->POST('/change_password', \App\Application\Actions\User\ChangePasswordAction::class);
  });

  $app->group(API_BASE_URL.'/memo_tag', function(Group $group) {
    $group->MAP(['GET', 'POST'], '/list[/{page}]', \App\Application\Actions\MemoTag\ListAction::class);
  });

  $app->group(API_BASE_URL.'/memo', function(Group $group) {
    $group->MAP(['GET', 'POST'], '/list[/{page}]', \App\Application\Actions\Memo\ListAction::class);
    $group->GET('/detail[/{id}]', \App\Application\Actions\Memo\DetailAction::class);
    $group->POST('/delete', \App\Application\Actions\Memo\DeleteAction::class);
    $group->POST('/save', \App\Application\Actions\Memo\SaveAction::class);
  });
};
