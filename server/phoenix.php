<?php
$settings = require_once(__DIR__ .'/config/config.php');
return [
  'migration_dirs' => [
    'dev' => __DIR__ . '/migrations',
    'test' => __DIR__.'/migrations_test'
  ],
  'environments' => [
    'dev' => $db,
    'test' => $test_db,
  ],
  'default_environment' => 'dev',
  'log_table_name' => 'phoenix_log',
];
