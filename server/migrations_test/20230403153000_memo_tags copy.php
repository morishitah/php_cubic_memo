<?php

use Phoenix\Migration\AbstractMigration;

class MemoTags extends AbstractMigration
{
  protected function up(): void
  {
    $this->execute("CREATE TABLE memo_tags (
          id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
          memo_id bigint unsigned NOT NULL COMMENT 'メモID',
          tag_id bigint unsigned NOT NULL COMMENT 'タグID',
          status int(4) NOT NULL default 0 COMMENT '状態:0:有効, 9:無効',
          created datetime NOT NULL COMMENT '作成日時',
          modified datetime NOT NULL COMMENT '更新日時',
          PRIMARY KEY (`id`)
      ) ENGINE=InnoDB COMMENT = 'メモとタグの中間テーブル';
    ");
    $this->execute("
      Insert Into memo_tags (memo_id, tag_id, status, created, modified) values
      (1, 1, 0, now(), now()),
      (1, 2, 0, now(), now()),
      (2, 2, 0, now(), now()),
      (2, 3, 0, now(), now()),
      (2, 4, 9, now(), now()),
      (3, 1, 0, now(), now()),
      (4, 1, 9, now(), now()),
      (4, 2, 0, now(), now()),
      (5, 1, 0, now(), now()),
      (5, 4, 0, now(), now())
    ");
  }

  protected function down(): void
  {
    $this->table('memo_tags')->drop();
  }
}
