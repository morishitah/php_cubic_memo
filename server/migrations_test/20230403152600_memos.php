<?php

use Phoenix\Migration\AbstractMigration;

class Memos extends AbstractMigration
{
  protected function up(): void
  {
    $this->execute("CREATE TABLE memos (
          id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
          user_id bigint unsigned NOT NULL COMMENT 'ユーザーID',
          label_id bigint unsigned NOT NULL COMMENT 'ラベルID',
          title varchar(100) NULL default '' COMMENT 'タイトル',
          contents varchar(2000) NULL default '' COMMENT 'メモ',
          share_flg int(4) NOT NULL default 0 COMMENT '共有フラグ:0:非共有, 1:共有',
          status int(4) NOT NULL default 0 COMMENT '状態:0:有効, 9:無効',
          created datetime NOT NULL COMMENT '作成日時',
          modified datetime NOT NULL COMMENT '更新日時',
          PRIMARY KEY (`id`)
      ) ENGINE=InnoDB COMMENT = 'メモ';
    ");
    $this->execute("
      Insert Into memos (user_id, label_id, title, contents, share_flg, status, created, modified) values
      (1, 1, 'タイトル1', 'メモ1', 1, 0, now(), now()),
      (1, 1, 'タイトル1', 'メモ1', 1, 0, now(), now()),
      (2, 1, 'タイトル1', 'メモ1', 1, 0, now(), now()),
      (2, 2, 'タイトル2', 'メモ2', 0, 0, now(), now()),
      (3, 2, 'タイトル3', 'メモ3', 0, 0, now(), now()),
      (4, 1, 'タイトル4', 'メモ4', 1, 0, now(), now()),
      (4, 1, 'タイトル1', 'メモ1', 1, 0, now(), now()),
      (5, 1, 'タイトル5', 'メモ5', 1, 9, now(), now()),
      (6, 2, 'タイトル6', 'メモ6', 0, 9, now(), now()),
      (6, 1, 'タイトル1', 'メモ1', 1, 0, now(), now())
    ");
  }

  protected function down(): void
  {
    $this->table('memos')->drop();
  }
}
