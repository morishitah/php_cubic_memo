<?php

use Phoenix\Migration\AbstractMigration;

class LabelTagDrop extends AbstractMigration
{
  protected function up(): void
  {
    $this->table('labels')->drop();
    $this->table('tags')->drop();
    $this->execute("ALTER TABLE memos DROP label_id");
    $this->execute("ALTER TABLE users DROP mailaddress");
    $this->execute("ALTER TABLE `memo_tags` CHANGE `tag_id` `name` VARCHAR(30) NOT NULL COMMENT 'タグ'");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 1");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 2");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 3");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 4");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 5");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 6");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 7");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 8");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 9");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 10");
  }
  
  protected function down(): void
  {
    $this->execute("CREATE TABLE labels (
          id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
          name varchar(100) NULL default '' COMMENT 'ラベル',
          status int(4) NOT NULL default 0 COMMENT '状態:0:有効, 9:無効',
          created datetime NOT NULL COMMENT '作成日時',
          modified datetime NOT NULL COMMENT '更新日時',
          PRIMARY KEY (`id`)
      ) ENGINE=InnoDB COMMENT = 'ラベル';
    ");
    $this->execute("
      Insert Into labels (name, status, created, modified) values
      ('ラベル1', 0, now(), now()),
      ('ラベル2', 0, now(), now()),
      ('ラベル3', 0, now(), now())
    ");
    $this->execute("CREATE TABLE tags (
          id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
          name varchar(30) NULL default '' COMMENT '名前',
          status int(4) NOT NULL default 0 COMMENT '状態:0:有効, 9:無効',
          created datetime NOT NULL COMMENT '作成日時',
          modified datetime NOT NULL COMMENT '更新日時',
          PRIMARY KEY (`id`)
      ) ENGINE=InnoDB COMMENT = 'タグ';
    ");
    $this->execute("
      Insert Into tags (name, status, created, modified) values
      ('タグ01', 0, now(), now()),
      ('タグ02', 0, now(), now()),
      ('タグ03', 0, now(), now()),
      ('タグ04', 0, now(), now()),
      ('タグ05', 9, now(), now()),
      ('タグ06', 0, now(), now())
    ");
    $this->execute("ALTER TABLE `users` ADD `mailaddress` varchar(256) NOT NULL COMMENT 'メールアドレス' AFTER `name`");
    $this->execute("ALTER TABLE `memos` ADD `label_id` BIGINT UNSIGNED NOT NULL COMMENT 'ラベルID' AFTER `user_id`");
    $this->execute("UPDATE `memo_tags` SET `name` = 1 WHERE `memo_tags`.`id` = 1");
    $this->execute("UPDATE `memo_tags` SET `name` = 2 WHERE `memo_tags`.`id` = 2");
    $this->execute("UPDATE `memo_tags` SET `name` = 2 WHERE `memo_tags`.`id` = 3");
    $this->execute("UPDATE `memo_tags` SET `name` = 3 WHERE `memo_tags`.`id` = 4");
    $this->execute("UPDATE `memo_tags` SET `name` = 4 WHERE `memo_tags`.`id` = 5");
    $this->execute("UPDATE `memo_tags` SET `name` = 1 WHERE `memo_tags`.`id` = 6");
    $this->execute("UPDATE `memo_tags` SET `name` = 1 WHERE `memo_tags`.`id` = 7");
    $this->execute("UPDATE `memo_tags` SET `name` = 2 WHERE `memo_tags`.`id` = 8");
    $this->execute("UPDATE `memo_tags` SET `name` = 1 WHERE `memo_tags`.`id` = 9");
    $this->execute("UPDATE `memo_tags` SET `name` = 4 WHERE `memo_tags`.`id` = 10");
    $this->execute("ALTER TABLE `memo_tags` CHANGE `name` `tag_id` bigint unsigned NOT NULL COMMENT 'タグID'");
  }
}
