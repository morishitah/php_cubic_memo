<?php

use Phoenix\Migration\AbstractMigration;

class Tags extends AbstractMigration
{
  protected function up(): void
  {
    $this->execute("CREATE TABLE tags (
          id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
          name varchar(30) NULL default '' COMMENT '名前',
          status int(4) NOT NULL default 0 COMMENT '状態:0:有効, 9:無効',
          created datetime NOT NULL COMMENT '作成日時',
          modified datetime NOT NULL COMMENT '更新日時',
          PRIMARY KEY (`id`)
      ) ENGINE=InnoDB COMMENT = 'タグ';
    ");
    $this->execute("
      Insert Into tags (name, status, created, modified) values
      ('タグ01', 0, now(), now()),
      ('タグ02', 0, now(), now()),
      ('タグ03', 0, now(), now()),
      ('タグ04', 0, now(), now()),
      ('タグ05', 9, now(), now()),
      ('タグ06', 0, now(), now())
    ");
  }

  protected function down(): void
  {
    $this->table('tags')->drop();
  }
}
