<?php

use Phoenix\Migration\AbstractMigration;

class Users extends AbstractMigration
{
  protected function up(): void
  {
    $this->execute("CREATE TABLE users (
          id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
          loginid varchar(30) NOT NULL COMMENT 'ログインID',
          name varchar(30) NULL default '' COMMENT '名前',
          mailaddress varchar(256) NOT NULL comment 'メールアドレス',
          password varchar(500) NOT NULL COMMENT 'パスワード',
          status int(4) NOT NULL default 0 COMMENT '状態:0:有効, 9:無効',
          created datetime NOT NULL COMMENT '作成日時',
          modified datetime NOT NULL COMMENT '更新日時',
          PRIMARY KEY (`id`)
      ) ENGINE=InnoDB COMMENT = 'ユーザー';
    ");
    $this->execute("
      Insert Into users (loginid, mailaddress, name, password, status, created, modified) values
      ('loginId1', 'test01@test.do.jp', '名前01', '".password_hash('test01', PASSWORD_BCRYPT, [9])."', 0, now(), now()),
      ('loginId3', 'test02@test.do.jp', '名前02', '".password_hash('test02', PASSWORD_BCRYPT, [9])."', 0, now(), now()),
      ('loginId4', 'test03@test.do.jp', '名前03', '".password_hash('test03', PASSWORD_BCRYPT, [9])."', 0, now(), now()),
      ('loginId5', 'test04@test.do.jp', '名前04', '".password_hash('test04', PASSWORD_BCRYPT, [9])."', 0, now(), now()),
      ('loginId6', 'test05@test.do.jp', '名前05', '".password_hash('test05', PASSWORD_BCRYPT, [9])."', 9, now(), now()),
      ('loginId7', 'test06@test.do.jp', '名前06', '".password_hash('test06', PASSWORD_BCRYPT, [9])."', 0, now(), now())
    ");
  }

  protected function down(): void
  {
    $this->table('users')->drop();
  }
}
