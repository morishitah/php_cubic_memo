<?php

use Phoenix\Migration\AbstractMigration;

class TagNameChange extends AbstractMigration
{
  protected function up(): void
  {
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 1");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ02' WHERE `memo_tags`.`id` = 2");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ02' WHERE `memo_tags`.`id` = 3");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ03' WHERE `memo_tags`.`id` = 4");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ04' WHERE `memo_tags`.`id` = 5");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ03' WHERE `memo_tags`.`id` = 6");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 7");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ05' WHERE `memo_tags`.`id` = 8");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ05' WHERE `memo_tags`.`id` = 9");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ07' WHERE `memo_tags`.`id` = 10");
  }
  
  protected function down(): void
  {
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 1");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 2");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 3");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 4");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 5");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 6");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 7");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 8");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 9");
    $this->execute("UPDATE `memo_tags` SET `name` = 'タグ01' WHERE `memo_tags`.`id` = 10");
  }
}
