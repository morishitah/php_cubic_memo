<?php

use Phoenix\Migration\AbstractMigration;

class LabelTagDrop extends AbstractMigration
{
  protected function up(): void
  {
    $this->table('labels')->drop();
    $this->table('tags')->drop();
    $this->execute("ALTER TABLE memos DROP label_id");
    $this->execute("ALTER TABLE users DROP mailaddress");
    $this->execute("ALTER TABLE `memo_tags` CHANGE `tag_id` `name` VARCHAR(30) NOT NULL COMMENT 'タグ'");
  }
  
  protected function down(): void
  {
    $this->execute("CREATE TABLE labels (
          id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
          name varchar(100) NULL default '' COMMENT 'ラベル',
          status int(4) NOT NULL default 0 COMMENT '状態:0:有効, 9:無効',
          created datetime NOT NULL COMMENT '作成日時',
          modified datetime NOT NULL COMMENT '更新日時',
          PRIMARY KEY (`id`)
      ) ENGINE=InnoDB COMMENT = 'ラベル';
    ");

    $this->execute("CREATE TABLE tags (
          id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
          name varchar(30) NULL default '' COMMENT '名前',
          status int(4) NOT NULL default 0 COMMENT '状態:0:有効, 9:無効',
          created datetime NOT NULL COMMENT '作成日時',
          modified datetime NOT NULL COMMENT '更新日時',
          PRIMARY KEY (`id`)
      ) ENGINE=InnoDB COMMENT = 'タグ';
    ");
    $this->execute("ALTER TABLE `users` ADD `mailaddress` varchar(256) NOT NULL COMMENT 'メールアドレス' AFTER `name`");
    $this->execute("ALTER TABLE `memos` ADD `label_id` BIGINT UNSIGNED NOT NULL COMMENT 'ラベルID' AFTER `user_id`");
    $this->execute("ALTER TABLE `memo_tags` CHANGE `name` `tag_id` bigint unsigned NOT NULL COMMENT 'タグID'");
  }
}
