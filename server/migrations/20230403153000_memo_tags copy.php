<?php

use Phoenix\Migration\AbstractMigration;

class MemoTags extends AbstractMigration
{
  protected function up(): void
  {
    $this->execute("CREATE TABLE memo_tags (
          id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
          memo_id bigint unsigned NOT NULL COMMENT 'メモID',
          tag_id bigint unsigned NOT NULL COMMENT 'タグID',
          status int(4) NOT NULL default 0 COMMENT '状態:0:有効, 9:無効',
          created datetime NOT NULL COMMENT '作成日時',
          modified datetime NOT NULL COMMENT '更新日時',
          PRIMARY KEY (`id`)
      ) ENGINE=InnoDB COMMENT = 'メモとタグの中間テーブル';
    ");
  }

  protected function down(): void
  {
    $this->table('memo_tags')->drop();
  }
}
