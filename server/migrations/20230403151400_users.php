<?php

use Phoenix\Migration\AbstractMigration;

class Users extends AbstractMigration
{
  protected function up(): void
  {
    $this->execute("CREATE TABLE users (
          id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
          loginid varchar(30) NOT NULL COMMENT 'ログインID',
          name varchar(30) NULL default '' COMMENT '名前',
          mailaddress varchar(256) NOT NULL comment 'メールアドレス',
          password varchar(500) NOT NULL COMMENT 'パスワード',
          status int(4) NOT NULL default 0 COMMENT '状態:0:有効, 9:無効',
          created datetime NOT NULL COMMENT '作成日時',
          modified datetime NOT NULL COMMENT '更新日時',
          PRIMARY KEY (`id`)
      ) ENGINE=InnoDB COMMENT = 'ユーザー';
    ");
    $this->execute("
      Insert Into users (loginid, mailaddress, name, password, status, created, modified) values
      ('nagamatsu', 'eiichi.nagamatsu@gmail.com', '永松', '".password_hash('Nagamatsu1', PASSWORD_BCRYPT, [9])."', 0, now(), now()),
      ('nakano', 'hidemaronakano0427@gmail.com', '中野', '".password_hash('Nakano1', PASSWORD_BCRYPT, [9])."', 0, now(), now()),
      ('morishita', 'morishita.0963@gmail.com', '森下', '".password_hash('Morishita1', PASSWORD_BCRYPT, [9])."', 0, now(), now())
    ");
  }

  protected function down(): void
  {
    $this->table('users')->drop();
  }
}
