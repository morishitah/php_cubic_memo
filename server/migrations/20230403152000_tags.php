<?php

use Phoenix\Migration\AbstractMigration;

class Tags extends AbstractMigration
{
  protected function up(): void
  {
    $this->execute("CREATE TABLE tags (
          id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
          name varchar(30) NULL default '' COMMENT '名前',
          status int(4) NOT NULL default 0 COMMENT '状態:0:有効, 9:無効',
          created datetime NOT NULL COMMENT '作成日時',
          modified datetime NOT NULL COMMENT '更新日時',
          PRIMARY KEY (`id`)
      ) ENGINE=InnoDB COMMENT = 'タグ';
    ");
  }

  protected function down(): void
  {
    $this->table('tags')->drop();
  }
}
