<?php

use Phoenix\Migration\AbstractMigration;

class MemosChangeContents extends AbstractMigration
{
  protected function up(): void
  {
    $this->execute("
      ALTER TABLE memos
      CHANGE contents contents LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'メモ';
    ");
  }

  protected function down(): void
  {
    $this->execute("
      ALTER TABLE memos
      CHANGE contents contents varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'メモ';
    ");
  }
}
