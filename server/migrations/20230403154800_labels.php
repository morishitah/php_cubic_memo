<?php

use Phoenix\Migration\AbstractMigration;

class Labels extends AbstractMigration
{
  protected function up(): void
  {
    $this->execute("CREATE TABLE labels (
          id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
          name varchar(100) NULL default '' COMMENT 'ラベル',
          status int(4) NOT NULL default 0 COMMENT '状態:0:有効, 9:無効',
          created datetime NOT NULL COMMENT '作成日時',
          modified datetime NOT NULL COMMENT '更新日時',
          PRIMARY KEY (`id`)
      ) ENGINE=InnoDB COMMENT = 'ラベル';
    ");
  }

  protected function down(): void
  {
    $this->table('labels')->drop();
  }
}
