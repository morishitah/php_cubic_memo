<?php
$db = [
  'adapter' => 'mysql',
  'host' => 'localhost',
  'port' => 3306, // optional
  'username' => 'root',
  'password' => '',
  'db_name' => 'system_db',
  'charset' => 'utf8',
];
$test_db = [
  'adapter' => 'mysql',
  'host' => 'localhost',
  'port' => 3306, // optional
  'username' => 'root',
  'password' => '',
  'db_name' => 'system_db_test',
  'charset' => 'utf8',
];
if (!defined('API_BASE_URL')) {
  define('API_BASE_URL', '/_api');
}
$develop_mode = true;
$mail = [
  'type' => 'smtp',
  'smtp' => 'smtp.mailtrap.io',
  'port' => 2525,
  'user_name' => '2a0202c9ff5e2c',
  'password' => '42c7b3093fb48d',
  'sendmail_command' => '/usr/sbin/sendmail -bs',
];
$template = [
  'mail' => dirname(__DIR__).'/template/mail',
];

// 内部暗号化・復号化関数
// 本来はコンテナ化したいが、テストでも直接使いたかったのでグローバル関数として定義
if (!function_exists('my_crypt')) {
  function my_crypt($val) {
      return openssl_encrypt($val, 'AES-128-ECB', 'kedfas3$DFI39)$8yvaludfsa39FD(484t4SD349e');
  }
}

if (!function_exists('my_decrypt')) {
  function my_decrypt($val) {
      return openssl_decrypt($val, 'AES-128-ECB', 'kedfas3$DFI39)$8yvaludfsa39FD(484t4SD349e');
  }
}

if (!function_exists('encodeSession')) {
  function encodeSession($data) {
      return base64_encode(serialize($data));
  }
}

if (!function_exists('decodeSession')) {
  function decodeSession($data) {
      return unserialize(base64_decode($data));
  }
}
$path = [
  'save_file' => dirname(__DIR__).'/public/',
  'publish_url' => 'http://192.168.33.3/img',
];
$deploy = [
    'url' => 'http://133.242.225.223/_api/deploy/update',
    'program_id' => 'sample',
    'is_active' => false,
];