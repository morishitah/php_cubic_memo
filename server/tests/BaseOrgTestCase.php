<?php

namespace Tests;

use ORM;
use Lcobucci\JWT\Parser;
use App\Application\Origin\JWT;
use App\Exception\JwtException;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Validation\Constraint\ValidAt;
use Lcobucci\JWT\Validation\Constraint\IssuedBy;
use App\Application\Middleware\SessionMiddleware;
use Lcobucci\JWT\Validation\Constraint\SignedWith;

abstract class BaseOrgTestCase extends TestCase
{
    public $useDB = true;
    public $url = '';
    public $app = '';
    public $method = '';
    public $isLogin = true;
    public $container = null;
    public $headers = null;

    public function __construct($name = null, array $data = [], $dataName = '') {
        if (!defined('IS_PHP_UNIT')) {
            define('IS_PHP_UNIT', true);
        }
        $this->app = $this->getAppInstance();
        $this->container = $this->app->getContainer();
//        $this->container = include(dirname(dirname(__DIR__)) . '/const/const.php');
//        $this->container['settings']['image_dir'] = $this->container['settings']['image_dir'].'test/';
//        $this->filepath = dirname(__DIR__).'/data/';
        parent::__construct($name, $data, $dataName);
    }

    public function runApp($requestMethod, $requestUri, $requestData = null) {
        $request = $this->createRequest($requestMethod, API_BASE_URL.$requestUri);
        if ($requestData) {
            $request = $request->withParsedBody(['data' => json_encode($requestData)]);
        }
        if ($this->headers) {
            foreach ($this->headers as $name => $header) {
                $request = $request->withHeader($name, $header);
            }
        }
        $response = $this->app->handle($request);

        if ($response->getStatusCode() != 200) {
            file_put_contents('./debug.html', (string)$response->getBody());
        }
        return $response;
    }

    protected function setUp() : void {
        parent::setUp();

        // DB初期化
        if ($this->useDB) {
            \ORM::configure([
                'connection_string' => sprintf('%s:host=%s;dbname=%s;port=%d;charset=utf8mb4', $this->container->get('settings')['test_db']['adapter'], $this->container->get('settings')['test_db']['host'], $this->container->get('settings')['test_db']['db_name'], $this->container->get('settings')['test_db']['port']),
                'username' => $this->container->get('settings')['test_db']['username'],
                'password' => $this->container->get('settings')['test_db']['password'],
                'caching' => false,
                'caching_auto_clear' => true,
            ]);

            \Model::$auto_prefix_models = '\App\Application\Models\\';;
            \Model::$short_table_names = true;
            \ORM::get_db()->beginTransaction();
        }
    }

    protected function tearDown() : void {
        parent::tearDown();
        $_SESSION = null;

        if (\ORM::get_db()->inTransaction()) {
            \ORM::get_db()->rollBack();
        }
        \ORM::get_db()->exec('ALTER TABLE users AUTO_INCREMENT=7');
        \ORM::get_db()->exec('ALTER TABLE memos AUTO_INCREMENT=11');
        \ORM::get_db()->exec('ALTER TABLE memo_tags AUTO_INCREMENT=11');


        // ID初期化
//        \ORM::get_db()->exec('ALTER TABLE user_points AUTO_INCREMENT=14');
        //$this->rmrf($this->container['settings']['image_dir']);
        //\ORM::clear_cache('prefectures');
        // \ORM::get_db()->exec("ALTER TABLE users AUTO_INCREMENT=11");
    }

    function rmrf($dir) {
        if (is_dir($dir) and !is_link($dir)) {
            foreach (glob($dir.'/*', GLOB_ONLYDIR) as $row) {
                $this->rmrf($row);
            }
            array_map('unlink', glob($dir.'/*'));
            rmdir($dir);
        }
    }

    function nowDate($date) {
        $c = $this->container->get('settings');
        $c['now_date'] = $date;
        $this->container->set('settings', $c);
    }

    function pageSize($page) {
        $c = $this->container->get('settings');
        $c['page_size'] = $page;
        $this->container->set('settings', $c);
    }

    protected function status200($response) {
        $this->assertSame(200, $response->getStatusCode());
        return json_decode((string)$response->getBody())->data;
    }

    protected function status302($response) {
        $this->assertSame(302, $response->getStatusCode());
        return $response->getHeaders()['Location'][0];
    }
    protected function statusNoLogin($response) {
        if ($response->getStatusCode() != 200) {
            $this->assertSame(302, $response->getStatusCode());
            $this->assertSame('/', $response->getHeaders()['Location'][0]);
        } else {
            $this->assertSame(200, $response->getStatusCode());
            $json = json_decode((string)$response->getBody());
            $this->assertSame(444, $json->data->state);
        }
    }

    protected function checkErrorsCount($json) {
        $cnt = 0;
        foreach ($json->errors as $row) {
            $cnt ++;
        }
        return $cnt;
    }

    protected function objectCount($json) {
        $cnt = 0;
        foreach ($json as $row) {
            $cnt ++;
        }
        return $cnt;
    }

    protected function login($id) {
        $jwt = new JWT($this->container);
        $token = $jwt->make(json_encode(['id' => $id]));

        $this->headers = ['token' => $token];
    }

    protected function userLogin($id = '1', $name = 'テストユーザー名01') {
        $c = $this->container->get('settings');
        $c['__session'] = [
            'id' => $id,
            'name' => $name,
        ];
        $this->container->set('settings', $c);
        // $this->session([
        //     'id' => $id,
        //     'name' => $name,
        // ]);
        // $_SESSION = [
        //     'id' => $id,
        //     'name' => $name,
        // ];
        // $jwt = new JWT($this->container);
        // $token = $jwt->make(json_encode(['id' => $id, 'name' => $name]));

        // $this->headers = ['token' => $token];
    }

    protected function parseToken($token) {
        $setting = $this->container->get('JWT');
        $config = Configuration::forSymmetricSigner(new Sha256(), InMemory::plainText($setting['token']));

        $uid = '';
        if ($token != '') {
            try {
                $token = $config->parser()->parse((string)$token);
            } catch (\Exception $e) {
                throw new \Exception('署名エラーです。');
            }
            $now = new \DateTimeImmutable();
            if ($token->isExpired($now)) {
                throw new \Exception('有効期限を確認してください。');
            }
            //制約を設定
            $setting = $this->container->get('JWT');
            $config->setValidationConstraints(new SignedWith($config->signer(), $config->signingKey()));
            $config->setValidationConstraints(new IssuedBy($setting['url']));
            if (!$config->validator()->validate($token, ...$config->validationConstraints())) {
                throw new \Exception('署名エラーです。');
            }
            $uid = $token->claims()->get('uid');
            if ($uid) {
                $uid = json_decode($uid);
            }
        }
        return $uid;
    }

    protected function cookie($response, $key) {
        $value = '';
        $cookies = $response->getHeader('Set-Cookie');
        foreach ($cookies as $cookie) {
            if (strpos('a'.$cookie, $key.'=') > 0) {
                $wks = explode(';', $cookie);
                $isHit = false;
                foreach ($wks as $wk) {
                    if (strpos($key.'=', $wk) >= 0) {
                        $wks = explode('=', $wk);
                        $value = urldecode(trim($wks[1]));
                        $isHit = true;
                        break;
                    }
                }
                if ($isHit) {
                    break;
                }
            }
        }

        return $value;
    }

    public function test_未ログイン()
    {
        if ($this->isLogin && $this->method != '') {
            $response = $this->runApp($this->method, $this->url);
            $json = $this->statusNoLogin($response);
        } else {
            $this->assertSame(true, true);
        }
    }

    protected function saveToken($value) {
        $model = \Model::factory('Consts');
        $row = $model->find_one(2);
        $row->date = '20210101';
        $row->str_value = $value;
        $row->save();
    }
}
