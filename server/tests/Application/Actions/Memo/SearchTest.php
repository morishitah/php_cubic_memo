<?php
declare(strict_types=1);

namespace Tests\Application\Actions\Memo;

use Tests\BaseOrgTestCase;

class SearchTest extends BaseOrgTestCase
{
    public $method = 'POST';
    public $url = '/memo/list';

    public function test_非共有メモ検索()
    {
        $model = \Model::factory('Memos');
        $query = $model->find_one(1);
        $query->share_flg = 0;
        $query->save();
        $param = [
            'title' => '',
            'share_flg' => '0',
            'user_id' => '',
            'tag' => '',
        ];
        $this->userLogin();
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);

        $json = $json->result;
        $this->assertSame(1, count($json->list));
        $rows = $json->list;
        $i = 0;
        $row = $rows[$i++];
        $this->assertSame('1', $row->id);
        $this->assertSame('1', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('0', $row->share_flg);
        $this->assertSame('タグ01', $row->tags[0]);
        $this->assertSame('タグ02', $row->tags[1]);
    }
    
    public function test_共有メモ検索()
    {
        $param = [
            'title' => '',
            'share_flg' => '1',
            'user_id' => '',
            'tag' => '',
        ];
        $this->userLogin();
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);
        
        $json = $json->result;
        $this->assertSame(6, count($json->list));
        $rows = $json->list;
        $i = 0;
        $row = $rows[$i++];
        $this->assertSame('10', $row->id);
        $this->assertSame('6', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame([], $row->tags);
        $row = $rows[$i++];
        $this->assertSame('7', $row->id);
        $this->assertSame('4', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame([], $row->tags);
        $row = $rows[$i++];
        $this->assertSame('6', $row->id);
        $this->assertSame('4', $row->user_id);
        $this->assertSame('タイトル4', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame([], $row->tags);
        $row = $rows[$i++];
        $this->assertSame('3', $row->id);
        $this->assertSame('2', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ03', $row->tags[0]);
        $row = $rows[$i++];
        $this->assertSame('2', $row->id);
        $this->assertSame('1', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ02', $row->tags[0]);
        $this->assertSame('タグ03', $row->tags[1]);
        $row = $rows[$i++];
        $this->assertSame('1', $row->id);
        $this->assertSame('1', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ01', $row->tags[0]);
        $this->assertSame('タグ02', $row->tags[1]);
    }
    
    public function test_タイトル検索()
    {
        $param = [
            'title' => 'タイトル1',
            'share_flg' => '1',
            'user_id' => '',
            'tag' => '',
        ];
        $this->userLogin();
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);

        $json = $json->result;
        $this->assertSame(5, count($json->list));
        $rows = $json->list;
        $i = 0;
        $row = $rows[$i++];
        $this->assertSame('10', $row->id);
        $this->assertSame('6', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame([], $row->tags);
        $row = $rows[$i++];
        $this->assertSame('7', $row->id);
        $this->assertSame('4', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame([], $row->tags);
        $row = $rows[$i++];
        $this->assertSame('3', $row->id);
        $this->assertSame('2', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ03', $row->tags[0]);
        $row = $rows[$i++];
        $this->assertSame('2', $row->id);
        $this->assertSame('1', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ02', $row->tags[0]);
        $this->assertSame('タグ03', $row->tags[1]);
        $row = $rows[$i++];
        $this->assertSame('1', $row->id);
        $this->assertSame('1', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ01', $row->tags[0]);
        $this->assertSame('タグ02', $row->tags[1]);
    }

    public function test_ユーザー検索()
    {
        $param = [
            'title' => '',
            'share_flg' => '1',
            'user_id' => '1',
            'tag' => '',
        ];
        $this->userLogin();
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);

        $json = $json->result;
        $this->assertSame(2, count($json->list));
        $rows = $json->list;
        $i = 0;
        $row = $rows[$i++];
        $this->assertSame('2', $row->id);
        $this->assertSame('1', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ02', $row->tags[0]);
        $this->assertSame('タグ03', $row->tags[1]);
        $row = $rows[$i++];
        $this->assertSame('1', $row->id);
        $this->assertSame('1', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ01', $row->tags[0]);
        $this->assertSame('タグ02', $row->tags[1]);
    }

    public function test_タグ検索()
    {
        $param = [
            'title' => '',
            'share_flg' => '1',
            'user_id' => '',
            'tag' => 'タグ02',
        ];
        $this->userLogin();
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);

        $json = $json->result;
        $this->assertSame(2, count($json->list));
        $rows = $json->list;
        $i = 0;
        $row = $rows[$i++];
        $this->assertSame('2', $row->id);
        $this->assertSame('1', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ02', $row->tags[0]);
        $this->assertSame('タグ03', $row->tags[1]);
        $row = $rows[$i++];
        $this->assertSame('1', $row->id);
        $this->assertSame('1', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ01', $row->tags[0]);
        $this->assertSame('タグ02', $row->tags[1]);
    }

    public function test_複数検索01()
    {
        $param = [
            'title' => 'タイトル1',
            'share_flg' => '1',
            'user_id' => '',
            'tag' => 'タグ03',
        ];
        $this->userLogin();
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);

        $json = $json->result;
        $this->assertSame(2, count($json->list));
        $rows = $json->list;
        $i = 0;
        $row = $rows[$i++];
        $this->assertSame('3', $row->id);
        $this->assertSame('2', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ03', $row->tags[0]);
        $row = $rows[$i++];
        $this->assertSame('2', $row->id);
        $this->assertSame('1', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ02', $row->tags[0]);
        $this->assertSame('タグ03', $row->tags[1]);
    }


    public function test_すべて()
    {
        $param = [
            'title' => '',
            'share_flg' => '1',
            'user_id' => '',
            'tag' => '',
        ];
        $this->userLogin();
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);

        $json = $json->result;
        $this->assertSame(6, count($json->list));
        $rows = $json->list;
        $i = 0;
        $row = $rows[$i++];
        $this->assertSame('10', $row->id);
        $this->assertSame('6', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame([], $row->tags);
        $row = $rows[$i++];
        $this->assertSame('7', $row->id);
        $this->assertSame('4', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame([], $row->tags);
        $row = $rows[$i++];
        $this->assertSame('6', $row->id);
        $this->assertSame('4', $row->user_id);
        $this->assertSame('タイトル4', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame([], $row->tags);
        $row = $rows[$i++];
        $this->assertSame('3', $row->id);
        $this->assertSame('2', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ03', $row->tags[0]);
        $row = $rows[$i++];
        $this->assertSame('2', $row->id);
        $this->assertSame('1', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ02', $row->tags[0]);
        $this->assertSame('タグ03', $row->tags[1]);
        $row = $rows[$i++];
        $this->assertSame('1', $row->id);
        $this->assertSame('1', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ01', $row->tags[0]);
        $this->assertSame('タグ02', $row->tags[1]);
    }
}