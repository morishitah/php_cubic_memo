<?php
declare(strict_types=1);

namespace Tests\Application\Actions\Memo;

use Tests\BaseOrgTestCase;

class DetailTest extends BaseOrgTestCase
{
  public $method = 'GET';
  public $url = '/memo/detail';

  public function test_詳細取得()
  {
    $this->userLogin();
    $response = $this->runApp($this->method, $this->url.'/1');

    $json = $this->status200($response);
    $this->assertSame(200, $json->result->state);

    $row = $json->result->result->detail;
    $this->assertSame('1', $row->id);
    $this->assertSame('1', $row->user_id);
    $this->assertSame('タイトル1', $row->title);
    $this->assertSame('メモ1', $row->contents);
    $this->assertSame('1', $row->share_flg);
    $this->assertSame('タグ01', $row->name[0]->name);
    $this->assertSame('タグ02', $row->name[1]->name);
  }

  public function test_詳細取得_別データ()
  {
    $this->userLogin();
    $response = $this->runApp($this->method, $this->url.'/3');

    $json = $this->status200($response);
    $this->assertSame(200, $json->result->state);

    $row = $json->result->result->detail;
    $this->assertSame('3', $row->id);
    $this->assertSame('2', $row->user_id);
    $this->assertSame('タイトル1', $row->title);
    $this->assertSame('メモ1', $row->contents);
    $this->assertSame('1', $row->share_flg);
    $this->assertSame('タグ03', $row->name[0]->name);
  }

  public function test_記事なし()
  {
    $this->userLogin();
    $response = $this->runApp($this->method, $this->url.'/99');

    $json = $this->status200($response);
    $this->assertSame(404, $json->result->state);
  }
}