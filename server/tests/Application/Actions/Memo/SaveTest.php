<?php
declare(strict_types=1);

namespace Tests\Application\Actions\Memo;

use Tests\BaseOrgTestCase;

class SaveTest extends BaseOrgTestCase
{
    public $method = 'POST';
    public $url = '/memo/save';

    public function test_新規登録()
    {
        $this->userLogin();
        $param = [
            'title' => 'タイトル',
            'contents' => 'メモ',
            'share_flg' => 0,
            'tags' => [
                'タグ01',
                'タグ02'
            ]
        ];
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);
        $this->assertSame(0, $this->checkErrorsCount($json));

        $model = \Model::factory('Memos');
        $this->assertSame(11, $model->count());
        $model = \Model::factory('MemoTags');
        $this->assertSame(12, $model->count());

        $model = \Model::factory('Memos');
        $row = $model->find_one(11);
        $this->assertSame('11', $row->id);
        $this->assertSame('タイトル', $row->title);
        $model = \Model::factory('MemoTags');
        $row = $model->find_one(11);
        $this->assertSame('11', $row->id);
        $this->assertSame('タグ01', $row->name);
        $model = \Model::factory('MemoTags');
        $row = $model->find_one(12);
        $this->assertSame('12', $row->id);
        $this->assertSame('タグ02', $row->name);
    }

    public function test_変更()
    {
        $this->userLogin();
        $param = [
            'id' => '3',
            'title' => 'タイトル',
            'contents' => 'メモ',
            'share_flg' => 0,
            'tags' => [
                'タグ01',
            ]
        ];
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);
        $this->assertSame(0, $this->checkErrorsCount($json));

        $model = \Model::factory('Memos');
        $this->assertSame(10, $model->count());
        $model = \Model::factory('MemoTags');
        $this->assertSame(10, $model->count());

        $model = \Model::factory('Memos');
        $row = $model->find_one(3);
        $this->assertSame('3', $row->id);
        $this->assertSame('タイトル', $row->title);
        $model = \Model::factory('MemoTags');
        $row = $model->find_one(11);
        $this->assertSame('11', $row->id);
        $this->assertSame('3', $row->memo_id);
        $this->assertSame('タグ01', $row->name);
    }

    public function test_新規登録エラー()
    {
        $this->userLogin();
        $param = [
            'title' => 'タイトル',
            'contents' => 'メモ',
            'tags' => []
        ];
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(204, $json->state);
        $this->assertSame(1, $this->checkErrorsCount($json));

        $model = \Model::factory('Memos');
        $this->assertSame(10, $model->count());
    }
}