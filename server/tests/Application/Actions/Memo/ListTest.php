<?php
declare(strict_types=1);

namespace Tests\Application\Actions\Memo;

use Tests\BaseOrgTestCase;

class ListTest extends BaseOrgTestCase
{

    public $method = 'GET';
    public $url = '/memo/list';

    public function test_一覧取得()
    {
        $this->userLogin();
        $response = $this->runApp($this->method, $this->url);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);
        
        $json = $json->result;
        $this->assertSame(6, count($json->list));
        $rows = $json->list;
        $i = 0;
        $row = $rows[$i++];
        $this->assertSame('10', $row->id);
        $this->assertSame('6', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame([], $row->tags);
        $row = $rows[$i++];
        $this->assertSame('7', $row->id);
        $this->assertSame('4', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame([], $row->tags);
        $row = $rows[$i++];
        $this->assertSame('6', $row->id);
        $this->assertSame('4', $row->user_id);
        $this->assertSame('タイトル4', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame([], $row->tags);
        $row = $rows[$i++];
        $this->assertSame('3', $row->id);
        $this->assertSame('2', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ03', $row->tags[0]);
        $row = $rows[$i++];
        $this->assertSame('2', $row->id);
        $this->assertSame('1', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ02', $row->tags[0]);
        $this->assertSame('タグ03', $row->tags[1]);
        $row = $rows[$i++];
        $this->assertSame('1', $row->id);
        $this->assertSame('1', $row->user_id);
        $this->assertSame('タイトル1', $row->title);
        $this->assertSame('1', $row->share_flg);
        $this->assertSame('タグ01', $row->tags[0]);
        $this->assertSame('タグ02', $row->tags[1]);
    }

    // public function test_ページャ()
    // {
    //     $c = $this->container->get('settings');
    //     $c['page_size'] = 1;
    //     $this->container->set('settings', $c);

    //     $this->userLogin();
    //     $response = $this->runApp($this->method, $this->url);
        
    //     $json = $this->status200($response);
    //     $this->assertSame(200, $json->state);

    //     $this->assertSame(1, count($json->result->list));
    //     $this->assertSame(6, $json->result->page->all);
    //     $this->assertSame(1, $json->result->page->start);
    //     $this->assertSame(1, $json->result->page->end);
    //     $this->assertSame(1, $json->result->page->next);
    //     $this->assertSame(1, $json->result->page->page);
    //     $this->assertSame(6, $json->result->page->all_page);
    //     $rows = $json->result->list;
    //     $i = 0;
    //     $row = $rows[$i];
    //     $this->assertSame('10', $row->id);
    // }

    // public function test_ページャ次のページ()
    // {
    //     $c = $this->container->get('settings');
    //     $c['page_size'] = 1;
    //     $this->container->set('settings', $c);

    //     $this->userLogin();
    //     $response = $this->runApp($this->method, $this->url.'/2');

    //     $json = $this->status200($response);
    //     $this->assertSame(200, $json->state);

    //     $this->assertSame(1, count($json->result->list));
    //     $this->assertSame(6, $json->result->page->all);
    //     $this->assertSame(2, $json->result->page->start);
    //     $this->assertSame(2, $json->result->page->end);
    //     $this->assertSame(1, $json->result->page->next);
    //     $this->assertSame(2, $json->result->page->page);
    //     $this->assertSame(6, $json->result->page->all_page);
    //     $rows = $json->result->list;

    //     $i = 0;
    //     $row = $rows[$i];
    //     $this->assertSame('7', $row->id);
    // }
}