<?php
declare(strict_types=1);

namespace Tests\Application\Actions\User;

use Tests\BaseOrgTestCase;

class ChangePasswordTest extends BaseOrgTestCase
{
    public $method = 'POST';
    public $url = '/user/change_password';

    public function test_パスワード変更()
    {
        $this->userLogin();
        $param = [
            'id' => '1',
            'password' => 'TestPass01',
        ];
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);
        $this->assertSame(0, $this->checkErrorsCount($json));

        $model = \Model::factory('Users');
        $this->assertSame(6, $model->count());

        $model = \Model::factory('Users');
        $row = $model->find_one(1);
        $this->assertSame('1', $row->id);
        $this->assertSame(true, password_verify('TestPass01', $row->password));
    }

    public function test_パスワード変更_別データ()
    {
        $this->userLogin();
        $param = [
            'id' => '3',
            'password' => 'TestPass02',
        ];
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);
        $this->assertSame(0, $this->checkErrorsCount($json));

        $model = \Model::factory('Users');
        $this->assertSame(6, $model->count());

        $model = \Model::factory('Users');
        $row = $model->find_one(3);
        $this->assertSame('3', $row->id);
        $this->assertSame(true, password_verify('TestPass02', $row->password));
    }
}