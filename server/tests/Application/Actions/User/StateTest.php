<?php
declare(strict_types=1);

namespace Tests\Application\Actions\User\Login;

use Tests\BaseOrgTestCase;

class StateTest extends BaseOrgTestCase
{
  public $method = 'GET';
  public $url = '/login/state';

  public function test_担当者状態取得()
  {
    $this->userLogin();
    $response = $this->runApp($this->method, $this->url);

    $json = $this->status200($response);

    $this->assertSame(200, $json->state);
    $this->assertSame('1', $json->result->id);
    $this->assertSame('テストユーザー名01', $json->result->name);
  }
}