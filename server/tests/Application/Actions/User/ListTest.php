<?php
declare(strict_types=1);

namespace Tests\Application\Actions\User;

use Tests\BaseOrgTestCase;

class ListTest extends BaseOrgTestCase
{
    public $method = 'GET';
    public $url = '/user/list';

    public function test_一覧取得()
    {
        $this->userLogin();
        $response = $this->runApp($this->method, $this->url);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);
        $this->assertSame(5, count($json->result->list));
        $rows = $json->result->list;
        $i = 0;
        $row = $rows[$i++];
        $this->assertSame('1', $row->id);
        $this->assertSame('loginId1', $row->loginid);
        $this->assertSame('名前01', $row->name);
        $row = $rows[$i++];
        $this->assertSame('2', $row->id);
        $this->assertSame('loginId3', $row->loginid);
        $this->assertSame('名前02', $row->name);
        $row = $rows[$i++];
        $this->assertSame('3', $row->id);
        $this->assertSame('loginId4', $row->loginid);
        $this->assertSame('名前03', $row->name);
        $row = $rows[$i++];
        $this->assertSame('4', $row->id);
        $this->assertSame('loginId5', $row->loginid);
        $this->assertSame('名前04', $row->name);
        $row = $rows[$i++];
        $this->assertSame('6', $row->id);
        $this->assertSame('loginId7', $row->loginid);
        $this->assertSame('名前06', $row->name);
    }

    
    public function test_ページャ()
    {
        $c = $this->container->get('settings');
        $c['page_size'] = 1;
        $this->container->set('settings', $c);

        $this->userLogin();
        $response = $this->runApp($this->method, $this->url);
        
        $json = $this->status200($response);
        $this->assertSame(200, $json->state);

        $this->assertSame(1, count($json->result->list));
        $this->assertSame(5, $json->result->page->all);
        $this->assertSame(1, $json->result->page->start);
        $this->assertSame(1, $json->result->page->end);
        $this->assertSame(1, $json->result->page->next);
        $this->assertSame(1, $json->result->page->page);
        $this->assertSame(5, $json->result->page->all_page);
        $rows = $json->result->list;
        $i = 0;
        $row = $rows[$i];
        $this->assertSame('1', $row->id);
    }

    public function test_ページャ次のページ()
    {
        $c = $this->container->get('settings');
        $c['page_size'] = 1;
        $this->container->set('settings', $c);

        $this->userLogin();
        $response = $this->runApp($this->method, $this->url.'/2');

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);

        $this->assertSame(1, count($json->result->list));
        $this->assertSame(5, $json->result->page->all);
        $this->assertSame(2, $json->result->page->start);
        $this->assertSame(2, $json->result->page->end);
        $this->assertSame(1, $json->result->page->next);
        $this->assertSame(2, $json->result->page->page);
        $this->assertSame(5, $json->result->page->all_page);
        $rows = $json->result->list;

        $i = 0;
        $row = $rows[$i];
        $this->assertSame('2', $row->id);
    }

}