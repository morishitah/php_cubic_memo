<?php
declare(strict_types=1);

namespace Tests\Application\Actions\User\Login;

use Tests\BaseOrgTestCase;

class LogoutTest extends BaseOrgTestCase
{
    public $method = 'GET';
    public $url = '/logout';

    public function test_ログアウト()
    {
        $this->userLogin(1);
        $response = $this->runApp($this->method, $this->url);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);
        $uid = $this->parseToken($json->_code);
        $this->assertSame($uid, '');
    }
}