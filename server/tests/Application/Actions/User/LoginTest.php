<?php
declare(strict_types=1);

namespace Tests\Application\Actions\User\Login;

use App\Application\Actions\ActionPayload;
use App\Domain\Admin\Login\LoginRepository;
use App\Domain\Admin\Login\Login;
use DI\Container;
use Tests\BaseOrgTestCase;

class LoginTest extends BaseOrgTestCase
{
  public $isLogin = false;
  public $method = 'POST';
  public $url = '/login';

  public function test_ログイン成功()
  {
    $param = [
      'loginid' => 'loginId1',
      'password' => 'test01',
    ];
    $response = $this->runApp($this->method, $this->url, $param);

    $json = $this->status200($response);
    $this->assertSame(200, $json->state);
    $this->assertSame(0, $this->checkErrorsCount($json));
    $this->assertSame($_SESSION['id'], '1');
    $this->assertSame($_SESSION['name'], '名前01');
  }

  public function test_パスワード違い()
  {
    $param = [
      'loginid' => 'loginId2',
      'password' => 'test02',
    ];
    $response = $this->runApp($this->method, $this->url, $param);

    $json = $this->status200($response);
    $this->assertSame(204, $json->state);
    $this->assertSame(1, $this->checkErrorsCount($json));
    $this->assertSame($_SESSION, null);
  }

  public function test_ログイン削除済み()
  {
    $param = [
      'loginid' => 'loginId4',
      'password' => 'test04',
    ];
    $response = $this->runApp($this->method, $this->url, $param);

    $json = $this->status200($response);
    $this->assertSame(204, $json->state);
    $this->assertSame(1, $this->checkErrorsCount($json));
    $this->assertSame($_SESSION, null);
  }

  public function test_ログイン存在しない()
  {
    $param = [
      'loginid' => 'loginId99',
      'password' => 'test99',
    ];
    $response = $this->runApp($this->method, $this->url, $param);

    $json = $this->status200($response);
    $this->assertSame(204, $json->state);
    $this->assertSame(1, $this->checkErrorsCount($json));
    $this->assertSame($_SESSION, null);
  }
}