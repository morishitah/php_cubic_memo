<?php
declare(strict_types=1);

namespace Tests\Application\Actions\User;

use Tests\BaseOrgTestCase;

class DetailTest extends BaseOrgTestCase
{
  public $method = 'GET';
  public $url = '/user/detail';

  public function test_詳細取得()
  {
    $this->userLogin();
    $response = $this->runApp($this->method, $this->url.'/1');

    $json = $this->status200($response);
    $this->assertSame(200, $json->result->state);
    $row = $json->result->result->detail;
    $this->assertSame('1', $row->id);
    $this->assertSame('loginId1', $row->loginid);
    $this->assertSame('名前01', $row->name);
  }

  public function test_詳細取得_別データ()
  {
    $this->userLogin();
    $response = $this->runApp($this->method, $this->url.'/3');

    $json = $this->status200($response);
    $this->assertSame(200, $json->result->state);

    $row = $json->result->result->detail;
    $this->assertSame('3', $row->id);
    $this->assertSame('loginId4', $row->loginid);
    $this->assertSame('名前03', $row->name);
  }

  public function test_記事なし()
  {
    $this->userLogin();
    $response = $this->runApp($this->method, $this->url.'/999');

    $json = $this->status200($response);
    $this->assertSame(404, $json->result->state);
  }
}