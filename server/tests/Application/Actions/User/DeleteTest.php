<?php
declare(strict_types=1);

namespace Tests\Application\Actions\User;

use Tests\BaseOrgTestCase;

class DeleteTest extends BaseOrgTestCase
{
    public $method = 'POST';
    public $url = '/user/delete';

    public function test_削除()
    {
        $this->userLogin();
        $param = [
            'id' => '1'
        ];
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);

        $model = \Model::factory('Users');
        $this->assertSame(6, $model->count());

        $model = \Model::factory('Users');
        $row = $model->find_one(1);
        $this->assertSame('1', $row->id);
        $this->assertSame('9', $row->status);
    }

    public function test_削除_別データ()
    {
        $this->userLogin();
        $param = [
            'id' => '3'
        ];
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);

        $model = \Model::factory('Users');
        $this->assertSame(6, $model->count());

        $model = \Model::factory('Users');
        $row = $model->find_one(1);
        $this->assertSame('1', $row->id);
        $this->assertSame('0', $row->status);
        
        $model = \Model::factory('Users');
        $row = $model->find_one(3);
        $this->assertSame('3', $row->id);
        $this->assertSame('9', $row->status);

        $model = \Model::factory('Users');
        $row = $model->find_one(5);
        $this->assertSame('5', $row->id);
        $this->assertSame('9', $row->status);
    }


}