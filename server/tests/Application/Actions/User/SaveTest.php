<?php
declare(strict_types=1);

namespace Tests\Application\Actions\User;

use Tests\BaseOrgTestCase;

class SaveTest extends BaseOrgTestCase
{
    public $method = 'POST';
    public $url = '/user/save';

    public function test_新規登録()
    {
        $this->userLogin();
        $param = [
            'loginid' => 'loginId7',
            'name' => 'テスト7',
            'password' => 'TestPass01',
        ];
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);
        $this->assertSame(0, $this->checkErrorsCount($json));

        $model = \Model::factory('Users');
        $this->assertSame(7, $model->count());

        $model = \Model::factory('Users');
        $row = $model->find_one(7);
        $this->assertSame('7', $row->id);
        $this->assertSame('loginId7', $row->loginid);
        $this->assertSame('テスト7', $row->name);
        $model = \Model::factory('Users');
        $row = $model->find_one(1);
        $this->assertSame('1', $row->id);
        $this->assertSame('loginId1', $row->loginid);
        $this->assertSame('名前01', $row->name);
    }

    public function test_変更()
    {
        $this->userLogin();
        $param = [
            'id' => '3',
            'loginid' => 'test03',
            'name' => 'テスト7',
        ];
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(200, $json->state);
        $this->assertSame(0, $this->checkErrorsCount($json));

        $model = \Model::factory('Users');
        $this->assertSame(6, $model->count());

        $model = \Model::factory('Users');
        $row = $model->find_one(3);
        $this->assertSame('3', $row->id);
        $this->assertSame('test03', $row->loginid);
        $this->assertSame('テスト7', $row->name);
    }

    public function test_新規登録エラー()
    {
        $this->userLogin();
        $param = [
            'loginid' => 'test03',
            'name' => 'テスト7',
            'password' => 'TestPass',
        ];
        $response = $this->runApp($this->method, $this->url, $param);

        $json = $this->status200($response);
        $this->assertSame(204, $json->state);
        $this->assertSame(1, $this->checkErrorsCount($json));

        $model = \Model::factory('Users');
        $this->assertSame(6, $model->count());
    }
}