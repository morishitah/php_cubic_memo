<?php
declare(strict_types=1);

namespace Tests\Model;

use Tests\BaseModelCase;

class MemoTagsTest extends BaseModelCase
{
  protected $modelName = 'MemoTags';

  
  public function test_memo_id()
  {
    $this->field = 'memo_id';
    $this->required($this->field, $this->setBack);
    $this->num($this->field, $this->setBack);
  }

  public function test_name()
  {
    $this->field = 'name';
    $this->required($this->field, $this->setBack);
    $this->max($this->field, 30, $this->setBack);
  }
}