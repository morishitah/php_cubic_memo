<?php
declare(strict_types=1);

namespace Tests\Model;

use Tests\BaseModelCase;

class MemosTest extends BaseModelCase
{
  protected $modelName = 'Memos';

  
  public function test_user_id()
  {
    $this->field = 'user_id';
    $this->required($this->field, $this->setBack);
    $this->num($this->field, $this->setBack);
  }
  
  public function test_label_id()
  {
    $this->field = 'label_id';
    $this->required($this->field, $this->setBack);
    $this->num($this->field, $this->setBack);
  }
  
  public function test_title()
  {
    $this->field = 'title';
    $this->required($this->field, $this->setBack);
    $this->max($this->field, 100, $this->setBack);
  }
  
  public function test_contents()
  {
    $this->field = 'contents';
    $this->required($this->field, $this->setBack);
  }
  
  public function test_share_flg()
  {
    $this->field = 'share_flg';
    $this->required($this->field, $this->setBack);
    $this->num($this->field, $this->setBack);
  }
}
