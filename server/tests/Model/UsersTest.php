<?php
declare(strict_types=1);

namespace Tests\Model;

use Tests\BaseModelCase;

class UsersTest extends BaseModelCase
{
  protected $modelName = 'Users';

  public function test_loginid()
  {
    $this->field = 'loginid';
    $this->required($this->field, $this->setBack);
    $this->max($this->field, 30, $this->setBack);
  }

  public function test_name()
  {
    $this->field = 'name';
    $this->required($this->field, $this->setBack);
    $this->max($this->field, 50, $this->setBack);
  }
  
  public function test_password()
  {
      $this->field = 'password';
      $this->required($this->field, $this->setBack);
      $this->max($this->field, 20, $this->setBack);
      $this->min($this->field, 8, $this->setBack);
      $this->password($this->field, $this->setBack);
  }
}