<?php
declare(strict_types=1);

namespace Tests;

abstract class BaseModelCase extends \PHPUnit\Framework\TestCase {
    protected $modelName = '';
    protected $validate = '';
    protected $setBack = '';
    protected $field = '';

    protected function setUp(): void {
        parent::setUp();

        // DB初期化
        $setting = include(dirname(__DIR__) . '/config/config.php');
        \ORM::configure([
            'connection_string' => sprintf('%s:host=%s;dbname=%s;port=%d;charset=utf8mb4', $test_db['adapter'], $test_db['host'], $test_db['db_name'], $test_db['port']),
            'username' => $test_db['username'],
            'password' => $test_db['password']
        ]);

        \Model::$auto_prefix_models = '\App\Application\Models\\';
        \Model::$short_table_names = true;

        $this->validate = new \App\Application\Origin\BaseValidate();

        $this->setBack = function($t, $val) {
            $field = $this->field;
            $t->$field = $val;
            return $t;
        };
    }

    /**
     * 必須入力チェック
     */
    protected function required($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();
        $t = $setBack($t, '');
        try {
            $t->validate();
            $this->assertSame('validate', 'error');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            $isHit = false;
            foreach ($message[$field] as $msg) {
                if ($this->validate->message['required'] == $msg) {
                    $isHit = true;
                    break;
                }
            }
            $this->assertSame(true, $isHit);
        }

        $t = $setBack($t, 'test');
        try {
            $t->validate();
            $this->assertSame('validate', 'validate');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            if (array_key_exists($field, $message)) {
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if ($this->validate->message['required'] == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * メールアドレスチェック
     */
    protected function mailaddress($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();
        $t = $setBack($t, 'test');
        try {
            $t->validate();
            $this->assertSame('validate', 'error');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            $isHit = false;
            foreach ($message[$field] as $msg) {
                if ($this->validate->message['email'] == $msg) {
                    $isHit = true;
                    break;
                }
            }
            $this->assertSame(true, $isHit);
        }

        $t = $setBack($t, 'test@aaaaaaa');
        try {
            $t->validate();
            $this->assertSame('validate', 'error');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            $isHit = false;
            foreach ($message[$field] as $msg) {
                if ($this->validate->message['email'] == $msg) {
                    $isHit = true;
                    break;
                }
            }
            $this->assertSame(true, $isHit);
        }

//        $t = $setBack($t, 'test@aaaaaaa.ooo');
//        try {
//            $t->validate();
//            $this->assertSame('validate', 'error');
//        } catch (\App\Application\Origin\ExceptionValidate $e) {
//            $message = $e->getErrorMessages();
//            $this->assertSame($this->validate->message['email'], $message[$field][0]);
//        }

        $t = $setBack($t, 'test@gmail.com');
        try {
            $t->validate();
            $this->assertSame('validate', 'validate');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            if (array_key_exists($field, $message)) {
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if ($this->validate->message['email'] == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * 半角英数字記号チェック
     */
    protected function alphanumkigo($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();
        $t = $setBack($t, 'Ｔest');
        try {
            $t->validate();
            $this->assertSame('validate', 'error');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            $isHit = false;
            foreach ($message[$field] as $msg) {
                if ($this->validate->message['alphaNumKigo'] == $msg) {
                    $isHit = true;
                    break;
                }
            }
            $this->assertSame(true, $isHit);
        }

        $t = $setBack($t, 'dafaあgeaa');
        try {
            $t->validate();
            $this->assertSame('validate', 'error');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            $isHit = false;
            foreach ($message[$field] as $msg) {
                if ($this->validate->message['alphaNumKigo'] == $msg) {
                    $isHit = true;
                    break;
                }
            }
            $this->assertSame(true, $isHit);
        }

        $t = $setBack($t, 'dafa2893@)(@#*$!@#=_geaa');
        try {
            $t->validate();
            $this->assertSame('validate', 'validate');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            if (array_key_exists($field, $message)) {
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if ($this->validate->message['alphaNumKigo'] == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * 最小文字列チェック
     */
    protected function min($field, $len, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $v = '';
        for ($i = 0; $i < $len - 1; $i ++) {
            $v .= 'a';
        }
        $t = $setBack($t, $v);
        try {
            $t->validate();
            $this->assertSame('validate', 'error');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            $isHit = false;
            foreach ($message[$field] as $msg) {
                if (sprintf($this->validate->message['min'], $len) == $msg) {
                    $isHit = true;
                    break;
                }
            }
            $this->assertSame(true, $isHit);
        }

        $v = '';
        for ($i = 0; $i < $len; $i ++) {
            $v .= 'a';
        }
        $t = $setBack($t, $v);
        try {
            $t->validate();
            $this->assertSame('validate', 'error');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            if (array_key_exists($field, $message)) {
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['min'], $len) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * 最大文字列チェック
     */
    protected function max($field, $len, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $v = '';
        for ($i = 0; $i < $len + 1; $i ++) {
            $v .= 'あ';
        }
        $t = $setBack($t, $v);
        try {
            $t->validate();
            $this->assertSame('validate', 'error');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            $isHit = false;
            foreach ($message[$field] as $msg) {
                if (sprintf($this->validate->message['max'], $len) == $msg) {
                    $isHit = true;
                    break;
                }
            }
            $this->assertSame(true, $isHit);
        }

        $v = '';
        for ($i = 0; $i < $len; $i ++) {
            $v .= 'a';
        }
        $t = $setBack($t, $v);
        try {
            $t->validate();
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            if (array_key_exists($field, $message)) {
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['max'], $len) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * 日時チェック
     */
    protected function datetime($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $errorPattern = [
            'afadi',
            '１９０９/01/01 10:10:00',
            '2019/01/01 4:06:1'
        ];
        foreach ($errorPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame('validate', 'error');
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['datetime']) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(true, $isHit);
            }
        }

        $successPattern = [
            '2019/01/01 04:06:00',
            '1919/12/31 23:59:59',
        ];
        foreach ($successPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame(true, true);
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                if (array_key_exists($field, $message)) {
                    foreach ($message[$field] as $msg) {
                        if (sprintf($this->validate->message['datetime']) == $msg) {
                            $isHit = true;
                            break;
                        }
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * 年月チェック
     */
    protected function ym($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $errorPattern = [
            'afadi',
            '１９０９/01',
            '2019/50'
        ];
        foreach ($errorPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame('validate', 'error');
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['ym']) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(true, $isHit);
            }
        }

        $successPattern = [
            '2019/01',
            '1919/12',
        ];
        foreach ($successPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame(true, true);
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                if (array_key_exists($field, $message)) {
                    foreach ($message[$field] as $msg) {
                        if (sprintf($this->validate->message['ym']) == $msg) {
                            $isHit = true;
                            break;
                        }
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * 日付チェック
     */
    protected function date($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $errorPattern = [
            'afadi',
            '１９０９/01/01',
            '2019/01/41'
        ];
        foreach ($errorPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame('validate', 'error');
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['datetime']) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(true, $isHit);
            }
        }

        $successPattern = [
            '2019/01/01',
            '1919/12/31',
        ];
        foreach ($successPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame(true, true);
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                if (array_key_exists($field, $message)) {
                    foreach ($message[$field] as $msg) {
                        if (sprintf($this->validate->message['datetime']) == $msg) {
                            $isHit = true;
                            break;
                        }
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * 時分チェック
     */
    protected function time($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $errorPattern = [
            'afadi',
            '10/10',
            '0406',
            '4:06',
            '25:06',
            '19:60',
        ];
        foreach ($errorPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame('validate', 'error');
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['time']) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(true, $isHit);
            }
        }

        $successPattern = [
            '04:06',
            '19:59',
            '23:59',
        ];
        foreach ($successPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame(true, true);
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                if (array_key_exists($field, $message)) {
                    foreach ($message[$field] as $msg) {
                        if (sprintf($this->validate->message['time']) == $msg) {
                            $isHit = true;
                            break;
                        }
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * 空白許可チェック
     */
    protected function allowblank($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $successPattern = [
            '',
        ];
        foreach ($successPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame(true, true);
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                if (array_key_exists($field, $message)) {
                    foreach ($message[$field] as $msg) {
                        if (sprintf($this->validate->message['required']) == $msg) {
                            $isHit = true;
                            break;
                        }
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * URLチェック
     */
    protected function url($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $errorPattern = [
            'afadi',
            'http://sssfdsa',
            'http://dfasfea/fdaie',
            'hts://test.co.jp/test.html',
            'https:/test.co.jp/test.html',
        ];
        foreach ($errorPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame('validate', 'error');
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['url']) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(true, $isHit);
            }
        }

        $successPattern = [
            'http://sdfasdfa.s',
            'http://あいうえお.co.jp',
            'http://test.co.jp/test',
            'https://test.co.jp/test.html',
        ];
        foreach ($successPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame(true, true);
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                if (array_key_exists($field, $message)) {
                    foreach ($message[$field] as $msg) {
                        if (sprintf($this->validate->message['url']) == $msg) {
                            $isHit = true;
                            break;
                        }
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * 数値チェック
     */
    protected function num($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $errorPattern = [
            'あ',
            '２',
            'a',
            '-',
        ];
        foreach ($errorPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame('validate', 'error');
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['num']) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(true, $isHit);
            }
        }

        $successPattern = [
            '12',
            '2',
            '-3',
            '300.23',
        ];
        foreach ($successPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame(true, true);
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                if (array_key_exists($field, $message)) {
                    foreach ($message[$field] as $msg) {
                        if (sprintf($this->validate->message['num']) == $msg) {
                            $isHit = true;
                            break;
                        }
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * 郵便番号
     */
    protected function zipcode($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $errorPattern = [
            'あ',
            '000-00000',
            '00-0000',
            '000-000',
            '000-a000',
            '5440022',
            '54400223',
            '544002',
        ];
        foreach ($errorPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame('validate', 'error');
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['zipcode']) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(true, $isHit);
            }
        }

        $successPattern = [
            '544-0022',
            '085-5569',
        ];
        foreach ($successPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame(true, true);
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                if (array_key_exists($field, $message)) {
                    foreach ($message[$field] as $msg) {
                        if (sprintf($this->validate->message['zipcode']) == $msg) {
                            $isHit = true;
                            break;
                        }
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * 電話番号(ハイフン無し)
     */
    protected function tel($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $errorPattern = [
            'あ',
            '000-000000000',
            '0000000-0000',
            '0000000000-0000-0000',
            'aaa-bbbb-cccc',
            '000-0000-000d',
            '000000000-00000000',
            '123456789',
        ];
        foreach ($errorPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame('validate', 'error');
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['tel']) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(true, $isHit);
            }
        }

        $successPattern = [
            '00000000000',
            '0000000000',
            '9999999999999',
            '9999985568',
        ];
        foreach ($successPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame(true, true);
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                if (array_key_exists($field, $message)) {
                    foreach ($message[$field] as $msg) {
                        if (sprintf($this->validate->message['tel']) == $msg) {
                            $isHit = true;
                            break;
                        }
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * 電話番号(ハイフンあり)
     */
    protected function tel_h($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $errorPattern = [
            'あ',
            '000-000000000',
            '0000000-0000',
            '0000000000-0000-0000',
            'aaa-bbbb-cccc',
            '000-0000-000d',
            '00000000000000000',
            '123456789',
        ];
        foreach ($errorPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame('validate', 'error');
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['tel']) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(true, $isHit);
            }
        }

        $successPattern = [
            '000-0000-0000',
            '00-0000-0000',
            '99999-9999-9999',
            '99999-85-568',
        ];
        foreach ($successPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame(true, true);
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                if (array_key_exists($field, $message)) {
                    foreach ($message[$field] as $msg) {
                        if (sprintf($this->validate->message['tel']) == $msg) {
                            $isHit = true;
                            break;
                        }
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * ひらがな
     */
    protected function hira($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $errorPattern = [
            'ア',
            'aaaaa',
            'アイうエオ',
            'あイうえお',
        ];
        foreach ($errorPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame('validate', 'error');
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['hira']) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(true, $isHit);
            }
        }

        $successPattern = [
            'あいうえお',
            'あいう　えお',
            'あいう えお',
            'ばゔぁろあ',
        ];
        foreach ($successPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame(true, true);
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                if (array_key_exists($field, $message)) {
                    foreach ($message[$field] as $msg) {
                        if (sprintf($this->validate->message['hira']) == $msg) {
                            $isHit = true;
                            break;
                        }
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * パスワード
     */
    protected function password($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $errorPattern = [
            '111111',
            'aaaaa',
            'AAAAA',
            'AAAAA22222',
            'a2',
        ];
        foreach ($errorPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame('validate', 'error');
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['password']) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(true, $isHit);
            }
        }

        $successPattern = [
            'Test0002',
            'estT0002',
            'TEjs9iE',
            'ss3T',
            'S3&a#@!$_()%'
        ];
        foreach ($successPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame(true, true);
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                if (array_key_exists($field, $message)) {
                    foreach ($message[$field] as $msg) {
                        if (sprintf($this->validate->message['password']) == $msg) {
                            $isHit = true;
                            break;
                        }
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }

    /**
     * カタカナ
     */
    protected function kana($field, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $errorPattern = [
            'あ',
            'aaaaa',
            'アイうエオ',
            'あいうえお',
        ];
        foreach ($errorPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame('validate', 'error');
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['kana']) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(true, $isHit);
            }
        }

        $successPattern = [
            'アイウエオ',
            'アイウ　エオ',
            'アイウ エオ',
            'バヴァロア',
        ];
        foreach ($successPattern as $v) {
            $t = $setBack($t, $v);
            try {
                $t->validate();
                $this->assertSame(true, true);
            } catch (\App\Application\Origin\ExceptionValidate $e) {
                $message = $e->getErrorMessages();
                $isHit = false;
                if (array_key_exists($field, $message)) {
                    foreach ($message[$field] as $msg) {
                        if (sprintf($this->validate->message['kana']) == $msg) {
                            $isHit = true;
                            break;
                        }
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }


    /**
     * ユニークチェック
     */
    protected function unique($field, $target, $okData, $targetID, $setBack) {
        $model = \Model::factory($this->modelName);
        $t = $model->create();

        $t = $setBack($t, $target);
        try {
            $t->validate();
            $this->assertSame('validate', 'error');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            $isHit = false;
            foreach ($message[$field] as $msg) {
                if (sprintf($this->validate->message['unique']) == $msg) {
                    $isHit = true;
                    break;
                }
            }
            $this->assertSame(true, $isHit);
        }

        $t = $setBack($t, $okData);
        try {
            $t->validate();
            $this->assertSame('validate', 'validate');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            if (array_key_exists($field, $message)) {
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['unique']) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }

        $t = $setBack($t, $target);
        $t->id = $targetID;
        try {
            $t->validate();
            $this->assertSame('validate', 'validate');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            if (array_key_exists($field, $message)) {
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['unique']) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }

        $t = $setBack($t, $okData);
        $t->id = $targetID;
        try {
            $t->validate();
            $this->assertSame('validate', 'validate');
        } catch (\App\Application\Origin\ExceptionValidate $e) {
            $message = $e->getErrorMessages();
            if (array_key_exists($field, $message)) {
                $isHit = false;
                foreach ($message[$field] as $msg) {
                    if (sprintf($this->validate->message['unique'], $len) == $msg) {
                        $isHit = true;
                        break;
                    }
                }
                $this->assertSame(false, $isHit);
            }
        }
    }
}