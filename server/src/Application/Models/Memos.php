<?php

namespace App\Application\Models;

use App\Application\Origin\BaseModel;
use \App\Application\Origin\ExceptionValidate;
use \App\Application\Origin\BaseValidate;

class Memos extends BaseModel {
  public $checkPassword = true;

  public function validate() {
    $v = new BaseValidate($this->as_array());
    $v->required(['user_id', 'title', 'contents', 'share_flg']);
    $v->max(['title'], 100);
    $v->num(['user_id', 'share_flg']);

    if (!$v->validate()) {
      $errors = $v->errors();
      throw new ExceptionValidate($errors);
    }
  }

  public function save() {
    try {
      $this->validate();
      parent::save();
    } catch (ExceptionValidate $e) {
      throw $e;
    }
  }

  public function j() {
    return [
      'id' => $this->id,
      'user_id' => $this->user_id,
      'title' => $this->title,
      'contents' => $this->contents,
      'share_flg' => $this->share_flg,
    ];
  }
}
