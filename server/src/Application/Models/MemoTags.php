<?php

namespace App\Application\Models;

use App\Application\Origin\BaseModel;
use \App\Application\Origin\ExceptionValidate;
use \App\Application\Origin\BaseValidate;

class MemoTags extends BaseModel {
  public function validate() {
    $v = new BaseValidate($this->as_array());
    $v->required(['memo_id', 'name']);
    $v->num(['memo_id']);
    $v->max(['name'], 30);

    if (!$v->validate()) {
      $errors = $v->errors();
      throw new ExceptionValidate($errors);
    }
  }

  public function save() {
    try {
      $this->validate();
      parent::save();
    } catch (ExceptionValidate $e) {
      throw $e;
    }
  }

  public function j() {
    return [
      'id' => $this->id,
      'memo_id' => $this->memo_id,
      'name' => $this->name,
    ];
  }
}