<?php

namespace App\Application\Models;

use App\Application\Origin\BaseModel;
use \App\Application\Origin\ExceptionValidate;
use \App\Application\Origin\BaseValidate;

class Users extends BaseModel {
  public $checkPassword = true;

  public function validate() {
    $v = new BaseValidate($this->as_array());
    $v->required(['loginid', 'name']);
    $v->max(['name'], 50);
    $v->max(['loginid'], 30);
    if ($this->checkPassword) {
      $v->required(['password']);
      $v->max(['password'], 20);
      $v->min(['password'], 8);
      $v->password(['password']);
    }

    if (!$v->validate()) {
      $errors = $v->errors();
      throw new ExceptionValidate($errors);
    }
  }

  public function save() {
    try {
      $this->validate();
      if ($this->checkPassword) {
        $this->password = password_hash(
            $this->password,
            $this->container->get('settings')['password']['user']['algo'],
            ['cost' => $this->container->get('settings')['password']['user']['cost']]
        );
    }
      parent::save();
    } catch (ExceptionValidate $e) {
      throw $e;
    }
  }

  public function j() {
    return [
      'id' => $this->id,
      'loginid' => $this->loginid,
      'name' => $this->name,
    ];
  }
}