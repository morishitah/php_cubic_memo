<?php
namespace App\Application\Dependence;

use \App\Application\Origin\ExceptionValidate;

class DataSaveDependence extends BaseData implements DataSaveInterface {
    protected $model = '';
    protected $controller;
    protected $paramCallback = null;
    protected $setterCallback = null;
    protected $saveParamData = null;
    protected $lastID = null;
    protected $setSort = false;
    protected $targetDate = null;

    public function init($name, $saveParamData = null, $setSort = false): void {
        $this->model = $name;
        $this->setSort = $setSort;
        $this->saveParamData = $saveParamData;
    }

    public function setContainer($c): void {
        $this->container = $c;
    }

    public function setParamCallback($callback): void {
        $this->paramCallback = $callback;
    }

    public function setSetterCallback($callback): void {
        $this->setterCallback = $callback;
    }

    public function getLastID() {
        return $this->lastID;
    }

    public function getTargetData() {
        return $this->targetData;
    }

    /**
     * 入力チェック
     */
    public function confirm(): array {
        $model = \Model::factory($this->model);
        $errorMessage = [];
        try {
            $postData = $this->postData;
            if ($this->paramCallback != null) {
                $postData = $this->paramCallback->__invoke($this->postData, $this->args);
            }
            if (property_exists($postData, 'id')) {
                $query = $model->find_one($postData->id);
            } else {
                $query = $model->create();
            }

            foreach ($postData as $k => $v) {
                $query->$k = $v;
            }
            if ($this->setterCallback) {
                $query = $this->setterCallback->__invoke($query, $postData);
            }
            $query->validate();
        } catch(ExceptionValidate $e) {
            $errorMessage = $e->getErrorMessages();
        }

        return ['errors' => $errorMessage];
    }

    public function execute($isIDNoneCreate = false): array {
        $model = \Model::factory($this->model);
        $errorMessage = [];

        $this->beginTransaction();
        try {
            $postData = $this->postData;

            if ($this->paramCallback != null) {
                $postData = $this->paramCallback->__invoke($this->postData, $this->args);
            }

            $this->save($model, $postData, $isIDNoneCreate, $this->saveParamData);

            $this->commit();
        } catch (ExceptionValidate $e) {
            $this->rollback();
            $errorMessage = $e->getErrorMessages();
        }

        return ['errors' => $errorMessage];
    }

    /**
     * @param $model
     * @param $postData
     * @param $isIDNoneCreate
     * @throws ExceptionValidate
     */
    private function save($model, $postData, $isIDNoneCreate, $field = null, $parentID = 0) {
        if (property_exists($postData, 'id')) {
            $query = $model->find_one($postData->id);
            if (!$query) {
                // 渡されたIDが存在しなければ特に何もしない。
                if (!$isIDNoneCreate) {
                    throw new ExceptionValidate([]);
                } else {
                    $query = $model->create();
                    $query->id = $postData->id;
                    $query->created = (new \DateTime())->format('Y/m/d H:i:s');
                }
            }
        } else {
            $query = $model->create();
        }

        foreach ($postData as $k => $v) {
            if ($this->saveParamData && array_key_exists($k, $this->saveParamData)) {
            } else {
                $query->$k = $v;
            }
        }

        if ($field && array_key_exists('_key', $field)) {
            $parentField = $field['_key'];
            $query->$parentField = $parentID;
        }

        if ($this->setterCallback) {
            $query = $this->setterCallback->__invoke($query, $postData, $field);
        }

        if ($query) {
            if (!$query->id && $this->setSort) {
                $row = $model->order_by_desc('id')->find_one();
                if ($row) {
                    $query->sort = $row->id + 1;
                } else {
                    $query->sort = 1;
                }
            }
            $errors = [];
            try {
                $query->save();
            } catch (ExceptionValidate $e) {
                $errors = $e->getErrorMessages();
            }
            $this->lastID = $query->id;
            $this->targetData = $query;

            if ($field) {
                foreach ($field as $k => $paramData) {
                    if (strpos('a'.$k, '_') > 0) {
                        continue;
                    }

                    if (array_key_exists('_before_delete', $paramData) && $paramData['_before_delete']) {
                        $m = \Model::factory($paramData['_model']);
                        $m->where($paramData['_key'], $query->id)->delete_many();
                    }
                    $postDatas = $postData->$k;
                    if (!is_array($postDatas)) {
                        $postDatas = [$postDatas];
                    }
                    foreach ($postDatas as $idx => $d) {
                        $m = \Model::factory($paramData['_model']);
                        try {
                            $this->save($m, $d, $isIDNoneCreate, $paramData, $query->id);
                        } catch (ExceptionValidate $e) {
                            if (!array_key_exists($paramData['_model'], $errors)) {
                                $errors[$paramData['_model']] = [];
                            }
                            $errors[$paramData['_model']][$idx] = $e->getErrorMessages();
                        }
                    }
                }
            }
            if (count($errors) > 0) {
                throw new ExceptionValidate($errors);
            }
        }
    }
}