<?php
namespace App\Application\Dependence;

use App\Application\Origin\ExceptionValidate;

class DataDeleteDependence extends BaseData implements DataDeleteInterface {
    protected $model = '';
    protected $controller;
    protected $request;
    protected $targetData = [];
    protected $childFields = null;
    protected $queryCallback = null;
    protected $resultCallback = null;

    public function init($name, $childFields = null, $queryCallback = null): void {
        $this->model = $name;
        $this->queryCallback = $queryCallback;
        $this->childFields = $childFields;
    }

    public function setContainer($c): void {
        $this->container = $c;
    }

    public function getData(): array {
        return $this->targetData;
    }

    public function setQueryCallback($callback): void {
        $this->queryCallback = $callback;
    }

    public function setResultCallback($callback): void {
        $this->resultCallback = $callback;
    }

    public function execute($isDelete = false, $isCommit = true): array {
        $errorMessage = [];

        $this->beginTransaction();
        try {
            $ids = $this->postData->id;
            if (!is_array($ids)) {
                $ids = [$ids];
            }

            foreach ($ids as $id) {
                $model = \Model::factory($this->model);
                $query = $model->where('id', $id);
                if ($this->queryCallback != null) {
                    $query = $this->queryCallback->__invoke($this->postData, $this->args, $query);
                }
                $query = $query->find_one();
                if (!$query) {
                    continue;
                }
                if ($this->resultCallback) {
                    $query = $this->resultCallback->__invoke($this->postData, $this->args, $query);
                }
                $this->targetData = $query->j();

                if (is_array($this->childFields)) {
                    foreach ($this->childFields as $r) {
                        $model = \Model::factory($r['model']);
                        $qs = $model
                            ->where($r['child_field'], $query->id)
                            ->find_many();
                        foreach ($qs as $q) {
                            if ($isDelete) {
                                $q->delete();
                            } else {
                                $q->status = 9;
                                $q->save();
                            }
                        }
                    }
                }

                if ($isDelete) {
                    $query->delete();
                } else {
                    $query->status = 9;
                    $query->save();
                }
            }

            if ($isCommit) {
                $this->commit();
            }
        } catch (ExceptionValidate $e) {
            $this->rollback();
            $errorMessage = $e->getErrorMessages();
        }
        return ['errors' => $errorMessage];
    }
}