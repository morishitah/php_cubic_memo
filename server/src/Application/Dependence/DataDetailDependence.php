<?php
namespace App\Application\Dependence;

class DataDetailDependence extends BaseData implements DataDetailInterface {

    protected $container;
    protected $model = '';
    protected $dispList = [];
    protected $queryCallback = null;

    public function init($name, $dispList): void {
        $this->model = $name;
        $this->dispList = $dispList;
    }

    public function setContainer($c): void {
        $this->container = $c;
    }

    public function setQueryCallback($callback): void {
        $this->queryCallback = $callback;
    }

    public function execute(): array {
        $state = 200;
        $id = $this->args['id'];
        $model = \Model::factory($this->model);
        $model = $model
            ->where($this->underscore($this->model).'.id', $id)
            ->where_not_equal($this->underscore($this->model).'.status', 9);
        if ($this->queryCallback != null) {
            $model = $this->queryCallback->__invoke($model);
        }
        $row = $model->find_array();
        if ($row) {
            $this->dispList = $this->setValues($this->dispList, $row[0]);
        } else {
            foreach ($this->dispList as $k => $v) {
                if ($v != '') {
                    $this->dispList[$k] = '';
                }
            }
            $state = 404;
        }

        return ['result' => ['detail' => $this->dispList], 'state' => $state];
    }

    private function setValues($ctrls, $row) {
        foreach ($ctrls as $k => $v) {
            if (is_callable($v)) {
                $ctrls[$k] = $v($row[$k]);
            } else if (is_array($v)) {
                $removeKey = $k;
                if (array_key_exists('model', $v)) {
                    $k = $v['model'];
                }
                $key = $this->underscore($k);
                if (array_key_exists('key', $v)) {
                    $key = $v['key'];
                }
                if ($v['mode'] == 'child') {
                    $model = \Model::factory($k);
                    $model = $model
                        ->where($v['child_field'], $row['id'])
                        ->where_not_equal('status', 9);
                    if (array_key_exists('order', $v)) {
                        $model = $model->order_by_expr($v['order']);
                    }
                    if (array_key_exists('where', $v)) {
                        $model = $v['where']($model);
                    }
                    $rs = $model->find_array();

                    $ctrls[$key] = [];
                    foreach ($rs as $r) {
                        $ctrls[$key][] = $this->setValues($v['field'], $r);
                    }
                    unset($ctrls[$removeKey]);
                }
            } else if (strpos($v, 'Y/m/d') === 0) {
                if ($row[$k] && $row[$k] != '') {
                    $date = new \DateTime($row[$k]);
                    $ctrls[$k] = $date->format($v);
                } else {
                    $ctrls[$k] = '';
                }
            } else if ($v === null) {
                $ctrls[$k] = '';
            } else {
                $ctrls[$k] = $row[$k];
            }
        }
        return $ctrls;
    }


}