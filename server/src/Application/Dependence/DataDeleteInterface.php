<?php
namespace App\Application\Dependence;

interface DataDeleteInterface
{
    public function init($name, $childFields = null, $queryCallback = null): void;
    public function setContainer($c): void;
    public function setParam($param): void;
    public function setArgs($args): void;
    public function getData(): array;
    public function setResultCallback($callback): void;
    public function execute($isDelete = false, $isCommit = true): array;
}