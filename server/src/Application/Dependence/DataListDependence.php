<?php
namespace App\Application\Dependence;

class DataListDependence extends BaseData implements DataListInterface {

    protected $model = '';
    protected $container;
    protected $queryCallback = null;
    protected $countCallback = null;
    protected $resultCallback = null;
    protected $requestParam = null;
    protected $defaultOrder = '';
    protected $showDelete = false;
    protected $pageSize = 0;

    public function init($name, $defOrder = 'id', $showDelete = false): void {
        $this->model = $name;
        $this->queryCallback = null;
        $this->defaultOrder = $defOrder;
        $this->showDelete = $showDelete;
    }

    public function setContainer($c): void {
        $this->container = $c;
    }

    public function setPageSize($page): void {
        $this->pageSize = $page;
    }

    public function setQueryCallback($requestParam, $callback): void {
        $this->requestParam = $requestParam;
        $this->queryCallback = $callback;
    }

    public function setCountCallback($callback): void {
        $this->countCallback = $callback;
    }

    public function setResultCallback($callback): void {
        $this->resultCallback = $callback;
    }

    public function execute($skipPager = false, $skipStatus = false): array {
        $page = [];

        $pageSize = $this->container->get('settings')['page_size'];
        if ($this->requestParam != null && property_exists($this->requestParam, 'page_size')) {
            $pageSize = $this->requestParam->page_size;
        }
        if (array_key_exists('page', $this->args)) {
            if ($this->args['page'] == 's') {
                $skipPager = true;
            }
        }

        if (!$skipPager) {
            $offset = 0;
            if (array_key_exists('page', $this->args)) {
                $offset = $this->args['page'] - 1;
            }

            $query = $this->getModel();
            $allCount = 0;
            if ($this->countCallback) {
                $allCount = $this->countCallback->__invoke($query, $this->requestParam);
            } else {
                $allCount = $query->count();
            }
            $page['all'] = $allCount;
            $page['page'] = $offset + 1;
            $page['start'] = ($offset * $pageSize) + 1;
            $page['end'] = $page['page'] * $pageSize;
            $page['next'] = 1;
            if ($page['end'] >= $page['all']) {
                $page['end'] = $page['all'];
                $page['next'] = 0;
            }
            $page['all_page'] = (int)($page['all'] / $pageSize) + (($page['all'] % $pageSize == 0 ? 0 : 1));
            if ($page['all'] == 0) {
                $page['start'] = 0;
            }
            # ページリスト
            $pages = [];
            $dispCount = 10;
            if ($page['all_page'] != 0) {
                $start = 1;
                $end = 0;
                if ($page['all_page'] < $dispCount) {
                    $end = $page['all_page'];
                } else if ($page['page'] < 6) {
                    $end = $dispCount;
                } else {
                    $start = $page['page'] - 6 + 1;
                    $end = $dispCount + $start - 1;
                    if ($end > $page['all_page']) {
                        $end = $page['all_page'];
                    }
                    $start = $end - $dispCount + 1;
                }
                for ($i = $start; $i <= $end; $i ++) {
                    $pages[] = $i;
                }
            }
            $page['pages'] = $pages;
        }

        $query = $this->getModel();
        if (!$skipPager) {
            $query = $query
                ->limit($pageSize)
                ->offset($offset * $pageSize);
        }

        $rows = $query->find_many();
        $result = [];
        if ($this->resultCallback) {
            $result = $this->resultCallback->__invoke($rows);
        } else {
            foreach ($rows as $row) {
                $row->setContainer($this->container);
                $result[] = $row->j();
            }
        }
        return ['page' => $page, 'list' => $result];
    }

    private function getModel() {
        $model = \Model::factory($this->model);
        $query = $model;
        if (!$this->showDelete) {
            $query = $query
                ->whereNotEqual($this->underscore($this->model) . '.status', 9);
        }

        $order = $this->defaultOrder;
        //if ($this->requestParam != null && array_key_exists('order', $this->requestParam)) {
        if ($this->requestParam != null && property_exists($this->requestParam, 'order')) {
            $order = $this->requestParam->order;
        }
        if ($order != '') {
            $wks = explode(' ', $order);
            if (count($wks) == 2) {
                $query->order_by_desc($wks[0]);
            } else {
                $query->order_by_asc($wks[0]);
            }
        }

        if ($this->queryCallback != null) {
            $query = $this->queryCallback->__invoke($this->requestParam, $query);
        }
        return $query;
    }

}