<?php
namespace App\Application\Dependence;

interface DataSaveInterface
{
    public function init($name, $saveParamData = null, $setSort = false): void;
    public function setContainer($c): void;
    public function setParam($param): void;
    public function setArgs($args): void;
    public function setParamCallback($callback): void;
    public function setSetterCallback($callback): void;
    public function getLastID();
    public function confirm(): array;
    public function execute($isIDNoneCreate = false): array;
}