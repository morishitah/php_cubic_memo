<?php
namespace App\Application\Dependence;

interface DataDetailInterface
{
    public function init($name, $dispList): void;
    public function setContainer($c): void;
    public function setParam($param): void;
    public function setArgs($args): void;
    public function execute(): array;
}