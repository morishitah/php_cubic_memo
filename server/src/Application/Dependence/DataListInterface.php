<?php
namespace App\Application\Dependence;

interface DataListInterface
{
    public function init($name, $defOrder = 'id', $showDelete = false): void;
    public function setContainer($c): void;
    public function setParam($param): void;
    public function setArgs($args): void;
    public function setQueryCallback($requestParam, $callback): void;
    public function setResultCallback($callback): void;
    public function execute($skipPager = false, $skipStatus = false): array;
}
