<?php
namespace App\Application\Dependence;

class BaseData {
    protected $postData;
    protected $args;
    
    public function setParam($param): void {
        $this->postData = $param;
    }

    public function setArgs($args): void {
        $this->args = $args;
    }

    protected function underscore($str) {
        return ltrim(strtolower(preg_replace('/[A-Z]/', '_\0', $str)), '_');
    }

    /**
     * トランザクション開始
     */
    public function beginTransaction() {
        if (!\ORM::get_db()->inTransaction()) {
            \ORM::get_db()->beginTransaction();
        }
    }

    /**
     * トランザクションコミット
     */
    public function commit() {
        if (!defined('IS_PHP_UNIT')) {
            \ORM::get_db()->commit();
        }
    }

    /**
     * トランザクションロールバック
     */
    public function rollback() {
        if (\ORM::get_db()->inTransaction()) {
            \ORM::get_db()->rollBack();
        }
        if (defined('IS_PHP_UNIT')) {
            $this->beginTransaction();
        }
    }
}