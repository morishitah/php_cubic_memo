<?php
namespace App\Application\Component;


class Resize {
	var $image;
	var $image_type;

	function _load($filename) {
		$image_info = @getimagesize($filename);
		if (!$image_info) {
			return false;
		}
		$img_type = $image_info[2];
		if($img_type == IMAGETYPE_JPEG ) {
			$image = imagecreatefromjpeg($filename);
		} elseif($img_type == IMAGETYPE_GIF ) {
			$image = imagecreatefromgif($filename);
		} elseif($img_type == IMAGETYPE_PNG ) {
			$image = imagecreatefrompng($filename);
			imagealphablending($image, false);
			imagesavealpha($image, true);
		} else {
			return false;
		}

		return [$img_type, $image];
	}

	function load($filename) {
		$result = $this->_load($filename);
		if (!$result) {
			return false;
		}
		$this->image_type = $result[0];
		$this->image = $result[1];

		// 画像回転処理
		try {
			$exif_data = @exif_read_data($filename);
			if(isset($exif_data['Orientation'])){
				$orientation = $exif_data['Orientation'];
				if($this->image){
					// 未定義
					if($orientation == 0){
						// 通常
					}else if($orientation == 1){
						// 左右反転
					}else if($orientation == 2){
						$this->image = $this->image_flop($this->image);
						// 180°回転
					}else if($orientation == 3){
						$this->image = $this->image_rotate($this->image, 180, 0);
						// 上下反転
					}else if($orientation == 4){
						$this->image = $this->image_Flip($this->image);
						// 反時計回りに90°回転 上下反転
					}else if($orientation == 5){
						$this->image = $this->image_rotate($this->image, 90, 0);
						$this->image = $this->image_flip($this->image);
						// 時計回りに90°回転
					}else if($orientation == 6){
						$this->image = $this->image_rotate($this->image, -90, 0);
						// 時計回りに90°回転 上下反転
					}else if($orientation == 7){
						$this->image = $this->image_rotate($this->image, -90, 0);
						$this->image = $this->image_flip($this->image);
						// 反時計回りに90°回転
					}else if($orientation == 8){
						$this->image = $this->image_rotate($this->image, 90, 0);
					}
				}
			}
		} catch (Exception $e) {
		}

		return true;
	}

    /**
     * バイナリで画像文字列を持っている場合これで初期化する
     */
	function setImage($binary) {
	    $this->image = imagecreatefromstring($binary);
    }

	function getExtension() {
		if($this->image_type == IMAGETYPE_JPEG) {
			return '.jpg';
		} elseif($this->image_type == IMAGETYPE_GIF) {
			return '.gif';
		} elseif( $this->image_type == IMAGETYPE_PNG ) {
			return '.png';
		}

		return '.bmp';
	}

    /**
     * 画像リソースからバイナリ文字列を取得する
     */
	function getImageString($mime = '', $compression = 100) {
        ob_start();
        if (strpos($mime, 'jpeg') > 0) {
            imagejpeg($this->image, null, $compression);
        } else if (strpos($mime, 'gif') > 0) {
            imagegif($this->image, null);
        } else {
            imagepng($this->image, null);
        }

        $imageBinary = ob_get_clean();
        return $imageBinary;
    }

	function grayscale() {
		imagefilter($this->image, IMG_FILTER_GRAYSCALE);
	}

	function save($filename, $image_type='', $compression=100, $permissions=null) {
		if ($image_type == '') {
			$image_type = $this->image_type;
		}

		if( $image_type == IMAGETYPE_JPEG ) {
			imagejpeg($this->image,$filename,$compression);
		} elseif( $image_type == IMAGETYPE_GIF ) {
			imagegif($this->image,$filename);
		} elseif( $image_type == IMAGETYPE_PNG ) {
			imagepng($this->image,$filename);
		}
		if( $permissions != null) {
			chmod($filename,$permissions);
		}
	}

	function output($image_type='') {
		if ($image_type == '') {
			$image_type = $this->image_type;
		}

		if( $image_type == IMAGETYPE_JPEG ) {
			imagejpeg($this->image);
		} elseif( $image_type == IMAGETYPE_GIF ) {
			imagegif($this->image);
		} elseif( $image_type == IMAGETYPE_PNG ) {
			imagepng($this->image);
		}
	}

	function getWidth() {
		return imagesx($this->image);
	}

	function getHeight() {
		return imagesy($this->image);
	}

	function resizeToSystem($width, $height) {
		$this->resizeToHeight($height);
	}

	function resizeToLargerBase($width, $height) {
		if ($this->getHeight() > $this->getWidth()) {
			$this->resizeToHeight($height);
		} else {
			$this->resizeToWidth($width);
		}
	}

	/**
	 * 画像比率から抜き出し方法を算出する。
	 */
	function resizeToPersent($resizedSize) {
		$basePer = $resizedSize[0] / $resizedSize[1];
		$pictPer = $this->getWidth() / $this->getHeight();
		if ($pictPer > $basePer) {
			$this->resizeToHeight($resizedSize[1]);
		} else {
			$this->resizeToWidth($resizedSize[0]);
		}
        $this->cutimage($resizedSize[0], $resizedSize[1]);
	}

	function resizeToHeight($height) {
 		if ($this->getHeight() < $height) {
 			return;
 		}

		// 指定サイズより小さい場合は無理やり拡張
		if ($this->getHeight() < $height) {
			$ratio = $height / $this->getHeight();
			$width = $this->getWidth() * $ratio;
			$this->resize($width,$height);
			return;
		}
		$ratio = $height / $this->getHeight();
		$width = $this->getWidth() * $ratio;
		$this->resize($width,$height);
	}

	function resizeToWidth($width, $height = 0) {
// 		// 指定サイズより小さい場合は無視
 		if ($this->getWidth() < $width) {
 			return;
 		}
		// 指定サイズより小さい場合は無理やり拡張
		if ($this->getWidth() < $width) {
			if ($height == 0) {
				$ratio = $width / $this->getWidth();
				$height = $this->getheight() * $ratio;
			}
			$this->resize($width,$height);
			return;
		}
		$ratio = $width / $this->getWidth();
		$height = $this->getheight() * $ratio;
		$this->resize($width,$height);
	}

	function scale($scale) {
		$width = $this->getWidth() * $scale/100;
		$height = $this->getheight() * $scale/100;
		$this->resize($width,$height);
	}

	function resize($width,$height) {
		$new_image = imagecreatetruecolor($width, $height);
		imagealphablending($new_image, false);
		imagesavealpha($new_image, true);
		imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
		$this->image = $new_image;
	}

	function resizeToCut($width, $height) {
		$this->resizeToLargerBase($width, $height);
		//$this->resizeToWidth($width);
		$this->cutimage($width, $height);
	}

	function cutimage($width, $height) {
        if ($this->getWidth() < $width && $this->getHeight() < $height) {
            return;
        }

		$new_image = imagecreatetruecolor($width, $height);
		//ブレンドモードを無効にする
		imagealphablending($this->image, false);
		imagealphablending($new_image, false);
		//完全なアルファチャネル情報を保存するフラグをonにする
		imagesavealpha($this->image, true);
		imagesavealpha($new_image, true);

		$backgroundColor = imagecolorallocatealpha($new_image, 255, 255, 255, 127);//背景色セット(透明)
		imagefill($new_image, 0, 0, $backgroundColor); // 背景を塗る。
		//imagecolortransparent($new_image,$backgroundColor);//透明化

		//$alpha = imagecolortransparent($this->img);  // 元画像から透過色を取得する
		//imagefill($new_image, 0, 0, $alpha);       // その色でキャンバスを塗りつぶす
		//imagecolortransparent($new_image, $alpha); // 塗りつぶした色を透過色として指定する

		//imagefill($new_image, 0, 0, $backImg);
		$sourceX = 0;
		$targetX = 0;
		$sourceWidth = 0;
		if ($width > $this->getWidth()) {
			$wk = $width - $this->getWidth();
			$targetX = $wk / 2;
			$sourceWidth = $this->getWidth();
		} else {
			$wk = $width - $this->getWidth();
			$targetX = $wk / 2;
			$sourceWidth = $this->getWidth() - $targetX;
		}
// 		if ($height > $this->getHeight()) {
// 			$height = $this->getHeight();
// 		}
		if ($height > $this->getHeight()) {
			$wk = $height - $this->getHeight();
			$targetY = $wk / 2;
			$sourceHeight = $this->getHeight();
		} else {
			$wk = $height - $this->getHeight();
			$targetY = $wk / 2;
			$sourceHeight = $this->getHeight() - $targetY;
		}
		//imagecopyresampled($new_image, $this->image, $targetX, 0, $sourceX, 0, $sourceWidth, $height, $sourceWidth, $height);
		imagecopyresampled($new_image, $this->image, $targetX, $targetY, $sourceX, 0, $sourceWidth, $sourceHeight, $sourceWidth, $sourceHeight);
		$this->image = $new_image;
	}

	// 画像の左右反転
	function image_flop($image){
		// 画像の幅を取得
		$w = imagesx($image);
		// 画像の高さを取得
		$h = imagesy($image);
		// 変換後の画像の生成（元の画像と同じサイズ）
		$destImage = @imagecreatetruecolor($w,$h);
		// 逆側から色を取得
		for($i=($w-1);$i>=0;$i--){
			for($j=0;$j<$h;$j++){
				$color_index = imagecolorat($image,$i,$j);
				$colors = imagecolorsforindex($image,$color_index);
				imagesetpixel($destImage,abs($i-$w+1),$j,imagecolorallocate($destImage,$colors["red"],$colors["green"],$colors["blue"]));
			}
		}
		return $destImage;
	}
	// 上下反転
	function image_flip($image){
		// 画像の幅を取得
		$w = imagesx($image);
		// 画像の高さを取得
		$h = imagesy($image);
		// 変換後の画像の生成（元の画像と同じサイズ）
		$destImage = @imagecreatetruecolor($w,$h);
		// 逆側から色を取得
		for($i=0;$i<$w;$i++){
			for($j=($h-1);$j>=0;$j--){
				$color_index = imagecolorat($image,$i,$j);
				$colors = imagecolorsforindex($image,$color_index);
				imagesetpixel($destImage,$i,abs($j-$h+1),imagecolorallocate($destImage,$colors["red"],$colors["green"],$colors["blue"]));
			}
		}
		return $destImage;
	}
	// 画像を回転
	function image_rotate($image, $angle, $bgd_color){
		return imagerotate($image, $angle, $bgd_color, 0);
	}
}
