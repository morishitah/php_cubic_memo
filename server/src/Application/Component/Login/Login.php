<?php
namespace App\Application\Component\Login;

use App\Application\Component\Cookie;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Login
{
  private $model = '';
  private $p = '';
  private $request;
  private $response;
  private $logger;

  public function __construct(String $modelName, Object $p, Request $request, Response &$response, $logger) {
    $this->model = $modelName;
    $this->p = $p;
    $this->request = $request;
    $this->response = $response;
    $this->logger = $logger;
  }

  public function execute(String $field, String $cookieName, Object $callback) {
    $model = \Model::factory($this->model);
    $adminData = $model
      ->where($field, $this->p->$field)
      ->where_not_equal('status', 9)
      ->find_many();
    $state = 204;
    $errorMessage = 'IDまたはパスワードが一致しません。';
    if (count($adminData) == 1) {
      if (password_verify($this->p->password, $adminData[0]->password)) {
        // ログイン成功
        $state = 200;
        $errorMessage = '';
        $cookie = new Cookie($this->request);
        if ($this->p != null && property_exists($this->p, 'save_cookie')) {
            if ($this->p->save_cookie == '1') {
                $this->response = $cookie->set($this->response, $cookieName, my_crypt($adminData[0]->id));
            }
        }
        if ($callback) {
          $callback($adminData[0]);
        }
      }
    }
    $param = ['state' => $state, 'errors' => []];
    if ($errorMessage != '') {
      $param['errors'] = ['loginid' => [$errorMessage]];
    }

    return $param;
  }

  public function getResponse() {
    return $this->response;
  }
}