<?php
namespace App\Application\Component\Transfer\Entity;

use \App\Application\Component\Transfer\TransferInterface;
use \App\Application\Component\Transfer\Template;

class Mail implements TransferInterface {
  private $to = [];
  private $attach = [];

  /**
   * 送信先追加
   */
  public function addTo(String $to): void {
    $this->to[] = $to;
  }

  /**
   * 添付ファイル追加
   */
  public function addAttach(String $filePath): void {
    $this->attach[] = $filePath;
  }

  /**
   * メール送信実行
   */
  public function execute(Template $template): void {
    $settings = $template->getSettings();

    if ($settings['mail']['type'] == 'smtp') {
      $transport = (new \Swift_SmtpTransport(
          $settings['mail']['smtp'],
          $settings['mail']['port']
        ))
        ->setUsername($settings['mail']['user_name'])
        ->setPassword($settings['mail']['password']);
    }

    if ($settings['mail']['type'] == 'sendmail') {
      $transport = new \Swift_SendmailTransport($settings['mail']['sendmail_command']);
    }

    $mailer = new \Swift_Mailer($transport);
    $message = (new \Swift_Message($template->getSubject()))
      ->setFrom([$settings['mail_setting']['from']])
      ->setTo($this->to)
      ->setBody($template->getBody());

    foreach ($this->attach as $attach) {
      $message->attach(\Swift_Attachment::fromPath($attach));
    }

    if (!defined('IS_PHP_UNIT')) {
      $mailer->send($message);
    }
  }
}