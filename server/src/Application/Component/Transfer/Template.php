<?php
namespace App\Application\Component\Transfer;

class Template
{
  private $settings = '';
  private $param = '';
  private $subject = '';
  private $body = '';
  private $layoutFileName = 'layout.txt';

  public function __construct(Array $settings, Object $param = null) {
    $this->settings = $settings;
    $this->param = $param;
  }

  public function getSettings(): Array {
    return $this->settings;
  }

  public function setLayout(String $layoutFileName): void {
    $this->layoutFileName = $layoutFileName;
  }

  // メールテンプレートからタイトル、本文の生成
  public function parseText(String $fileName): void {
    $file = file($this->settings['template']['mail'].'/'.$fileName);
    foreach ($file as $k => $row) {
      if ($k == 0) {
        $this->subject = trim($row);
      } else {
        $this->body .= $row;
      }
    }

    if ($this->param) {
      foreach ($this->param as $k => $row) {
        $this->body = str_replace('{{'.$k.'}}', $row, $this->body);
      }
    }

    // メールレイアウトファイル取得
    $layoutPath = $this->settings['template']['mail'].'/'.$this->layoutFileName;
    if (file_exists($layoutPath)) {
      $layoutText = file_get_contents($layoutPath);
      $this->body = str_replace('{{__text__}}', $this->body, $layoutText);
    }
  }

  public function getSubject(): String {
    return $this->subject;
  }

  public function getBody(): String {
    return $this->body;
  }
}