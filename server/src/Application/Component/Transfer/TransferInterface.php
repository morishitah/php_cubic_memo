<?php
namespace App\Application\Component\Transfer;

interface TransferInterface
{
  /**
   * 送信先を追加
   */
  public function addTo(String $to): void;

  /**
   * 添付ファイルを追加
   */
  public function addAttach(String $to): void;

  /**
   * 送信実行
   */
  public function execute(Template $template): void;
}