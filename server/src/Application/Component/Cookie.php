<?php
namespace App\Application\Component;

/**
 * Cookie管理クラス
 */
class Cookie {
    protected $request = '';
    protected $limitMinute = 60 * 24 * 30;

    public function __construct($request = '') {
        $this->request = $request;
    }

    public function get($key, $defaultValue = '') {
        return $this->request->getCookieParams($key, '');
        // $cookie = $this->request->getCookieParams('code', '');
        // if (isset($cookie[$key])) {
        //     return $cookie[$key];
        // }
        // return $defaultValue;
    }

    public function set($response, $key, $val = '') {
        $expiry = new \DateTimeImmutable('now + '. $this->limitMinute . 'minutes');
        $cookie = urlencode($key) . '=' . urlencode($val)
            .'; expires=' . $expiry->format(\DateTime::COOKIE)
            .'; Max-Age=' . $this->limitMinute * 60
            .'; path=/'
            // .'; httponly'
        ;
        return $response->withAddedHeader('Set-Cookie', $cookie);
    }

    public function delete($response, $key)
    {
        $cookie = urlencode($key) . '=' . urlencode('') .
            '; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0; path=/; httponly';
        $response = $response->withAddedHeader('Set-Cookie', $cookie);
        return $response;
    }
}