<?php

namespace App\Application\Origin;

// https://github.com/vlucas/valitron
class BaseValidate extends \Valitron\Validator {

    public $message = [
        //'required' => '{field}は必須入力です。'
        'required' => '入力してください',
        'num' => '形式が不正です',
        'email' => '形式が不正です',
        'alphaNum' => '半角英数字で入力してください',
        'alphaNumKigo' => '半角英数記号で入力してください',
        'password' => '半角英字大文字、小文字、数字を含めてください',
        'max' => '%d文字以内で入力してください',
        'min' => '%d文字以上で入力してください',
        'unique' => '既に使われています',
        'datetime' => '形式が不正です',
        'url' => '形式が不正です',
        'tel' => 'ハイフン(-)を含んだ半角数値で入力してください',
        'zipcode' => 'ハイフン(-)を含んだ半角数値で入力してください',
        'kana' => 'カタカナで入力してください',
    ];

    /**
     * 必須入力
     */
    public function required($fields) {
        $this
            ->rule('required', $fields)
            ->message($this->message['required']);
    }

    /**
     * 数値入力
     */
    public function num($fields) {
        $this
            ->rule('numeric', $fields)
            ->message($this->message['num']);
    }

    /**
     * 郵便番号
     */
    public function zipcode($fields) {
        $this
            ->rule('regex', $fields, '/^([0-9]{3})-([0-9]{4})$/')
            // ->rule('regex', $fields, '/^([0-9]{7})$/')
            ->message($this->message['zipcode']);
    }

    /**
     * メールアドレス
     */
    public function mailaddress($fields) {
        $this
            //->rule('emailDNS', $fields)
            ->rule('email', $fields)
            ->message($this->message['email']);
    }

    /**
     * 半角英数記号
     */
    public function alphaNumKigo($fields) {
        $this
            ->rule('regex', $fields, '/^[!-~\s]+$/')
            ->message($this->message['alphaNumKigo']);
    }

    /**
     * カタカナ
     */
    public function kana($fields) {
        $this
            ->rule('regex', $fields, '/^[ァ-ヶー 　]+$/u')
            ->message($this->message['kana']);
    }

    /**
     * カタカナ+英数字記号
     */
    public function kanaalphakigo($fields) {
        $this
            ->rule('regex', $fields, '/^[!-~\sァ-ヶー 　]+$/u')
            ->message($this->message['kana']);
    }

    /**
     * 電話番号
     */
    public function tel($fields) {
        $this
            //->rule('regex', $fields, '/^([0-9]{2,5})-([0-9]{2,4})-([0-9]{1,4})$/')
            ->rule('regex', $fields, '/^([0-9]{10,16})$/')
            ->message($this->message['tel']);
    }
    /**
     * 電話番号(ハイフンあり)
     */
    public function tel_h($fields) {
        $this
            ->rule('regex', $fields, '/^([0-9]{2,5})-([0-9]{2,4})-([0-9]{1,4})$/')
            ->message($this->message['tel']);
    }

    /**
     * 最小文字列チェック
     */
    public function min($fields, $len) {
        $this
            ->rule('lengthMin', $fields, $len)
            ->message($this->message['min']);
    }

    /**
     * 最大文字列チェック
     */
    public function max($fields, $len) {
        $this
            ->rule('lengthMax', $fields, $len)
            ->message($this->message['max'], $len);
    }

    /**
     * 日時チェック
     */
    public function datetime($fields) {
        // YYYY/mm/dd HH:ii
        $this
            ->rule('regex', $fields, '/^([1-9][0-9]{3})[\/|-](0[1-9]{1}|1[0-2]{1})[\/|-](0[1-9]{1}|[1-2]{1}[0-9]{1}|3[0-1]{1}) ([0-1][0-9]{1}|2[0-3]{1}):([0-5][0-9]{1}):([0-5][0-9]{1})$/')
            ->message($this->message['datetime']);
    }

    /**
     * 日付チェック
     */
    public function date($fields) {
        // YYYY/mm/dd HH:ii
        $this
            ->rule('regex', $fields, '/^([1-9][0-9]{3})[\/|-](0[1-9]{1}|1[0-2]{1})[\/|-](0[1-9]{1}|[1-2]{1}[0-9]{1}|3[0-1]{1})$/')
            ->message($this->message['datetime']);
    }

    /**
     * URLチェック
     */
    public function url($fields) {
        $this
            ->rule('regex', $fields, '/(?:^|[\s　]+)((?:https?):\/\/[^\f\n\r\t\v　]+\.[^\f\n\r\t\v　]+)/u')
            ->message($this->message['url']);
    }

    /**
     * パスワードチェック
     */
    public function password($fields) {
        $this
            ->rule('regex', $fields, '/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])[a-zA-Z0-9]/u')
            ->message($this->message['password']);
    }


    /**
     * ユニークチェック
     */
    public function unique($fields, $modelName, $myID) {
        $this
            ->rule(function($field, $value, $params, $fields) {
                $model = \Model::factory($params[0]);
                $admin = $model
                    ->where($field, $value)
                    ->where_not_equal('status', 9)
                    ->find_one()
                ;
                if (!$admin) {
                    return true;
                } else if ($params[1]) {
                    if ($params[1] == $admin->id) {
                        return true;
                    }
                }
                return false;
        }, $fields, $modelName, $myID)->message($this->message['unique']);
    }

    /**
     * ユニークチェック(ステータス9は含めない)
     */
    public function uniqueNotStatus9($fields, $modelName, $myID) {
        $this
            ->rule(function($field, $value, $params, $fields) {
                $model = \Model::factory($params[0]);
                $admin = $model->where($field, $value)->where_not_equal('status', 9)->find_one();
                if (!$admin) {
                    return true;
                } else if ($params[1]) {
                    if ($params[1] == $admin->id) {
                        return true;
                    }
                }
                return false;
        }, $fields, $modelName, $myID)->message($this->message['unique']);
    }
}