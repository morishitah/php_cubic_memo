<?php
namespace App\Application\Origin;

use App\Application\Actions\Action;
use App\Exception\LoginErrorException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use App\Application\Component\Mail;

class BaseAction extends Action{
    public $c;
    private $jwt;
    private $jwtCode = '';
    public $p = null;
    public $session = null;
    public $checkLogin = true;  // ログインチェックを行うか
    protected $component;
    protected $args;
    public $imgUpload = false;

    public function __construct(LoggerInterface $logger, ContainerInterface $c)
    {
        try {
            parent::__construct($logger, $c);
            $this->c = $c;
            $this->jwt = new JWT($c);
            if (!defined('IS_PHP_UNIT')) {
                \ORM::configure([
                    'connection_string' => sprintf('%s:host=%s;dbname=%s;port=%d;charset=utf8mb4', $this->c->get('settings')['db']['adapter'], $this->c->get('settings')['db']['host'], $this->c->get('settings')['db']['db_name'], $this->c->get('settings')['db']['port']),
                    'username' => $this->c->get('settings')['db']['username'],
                    'password' => $this->c->get('settings')['db']['password'],
                    'caching' => true,
                    'caching_auto_clear' => true,
                ]);

                \Model::$auto_prefix_models = '\App\Application\Models\\';
                \Model::$short_table_names = true;
            }

            if ($this->c->get('settings')['development']) {
                \ORM::configure('logging', true);
                \ORM::configure('logger', function ($log_string, $query_time) use ($c) {
                    // $log_string にSQLクエリが、$query_timeに処理時間が入ります。
                    file_put_contents($c->get('settings')['sql_logger']['path'], $log_string . ' in ' . $query_time . "\n", FILE_APPEND);
                });
            }
            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }
        } catch (\Exception $e) {
        }
    }

    public function __invoke(Request $request, Response $response, $args): Response
    {
        try {
            $this->jwtCode = $this->jwt->check($request);
            $this->args = $args;
            // //POST
            // $param = $request->getParsedBody();
            // if ($param && array_key_exists('data', $param)) {
            //     $jsonString = $param['data'];
            //     $this->p = json_decode($jsonString);
            // }
            
            // テストの時のJSONになるように後々修正
            if (defined('IS_PHP_UNIT')) {
                $param = $request->getParsedBody();
                if ($param && array_key_exists('data', $param)) {
                    $jsonString = $param['data'];
                    $this->p = json_decode($jsonString);
                }
                $settings = $this->c->get('settings');
                if (array_key_exists('__session', $settings)) {
                    $_SESSION = $this->c->get('settings')['__session'];
                }
            } else {
                $param = json_decode(file_get_contents('php://input'));
                if ($param) {
                    if (gettype($param) == 'object') {
                        $this->p = $param;
                    } else {
                        $this->response = $response;
                        return $this->respondWithData(['state' => 500]);
                    }
                }
            }

            if ($this->component) {
                $this->component->setParam($this->p);
                $this->component->setArgs($args);
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        return parent::__invoke($request, $response, $args);
    }

    protected function checkLogin() {
        // if ($this->jwtCode == '') {
        //     if ($this->checkLogin) {
        //         throw new LoginErrorException('');
        //     }
        // } else {
        //     $this->session = json_decode($this->jwtCode);
        // }
        if ($this->checkLogin) {
            if (!$_SESSION || !array_key_exists('id', $_SESSION) || !$_SESSION['id']) {
                throw new LoginErrorException('');
            }
        }
    }

    /**
     * セッションに書き込む
     */
    protected function writeSession($key, $value) {
        $_SESSION[$key] = $value;
        // if (!$this->session) {
        //     $this->session = [];
        // } else {
        //     $this->session = (array)$this->session;
        // }
        // $this->session[$key] = $value;
    }

    /**
     * セッションをクリアする
     */
    protected function clearSession() {
        $this->session = null;
    }

    //実行
    protected function action(): Response
    {
        return $this->respondWithData([]);
    }

    /**
     * Like エスケープを実施
     */
    public function likeString($s) {
        return "%" . mb_ereg_replace('([_%#])', '#\1', $s) . "%";
    }

    /**
     * ランダムな文字列を生成する
     * 最大長は36文字まで
     */
    protected function randomStr($len = 8) {
        return substr(
            str_shuffle('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_'),
            0,
            $len
        );
    }

    /**
     * 日付文字列をフォーマットする
     */
    protected function formatDate($val, $format="Y/m/d") {
        if (!$val) {
            return '';
        }
        return (new \DateTime($val))->format($format);
    }

    public function replaceNullToBlank($val) {
        if ($val === 0 || $val === '0') {
            return $val;
        }
        return $val ? $val : '';
    }

    public function nowDate() {
        if (array_key_exists('now_date', $this->c->get('settings'))) {
            return new \DateTime($this->c->get('settings')['now_date']);
        }
        return new \DateTime();
    }

    protected function r($param): Response {
        if (!array_key_exists('state', $param)) {
            $param['state'] = 200;
        }
        if (array_key_exists('errors', $param)) {
            if (count($param['errors']) > 0) {
                $param['state'] = 204;
            } else {
                $param['errors'] = (object)[];
            }
        }
        $jwtCode = '';
        if ($this->session) {
            $jwtCode = json_encode($this->session);
        }

        $param['_code'] = $this->jwt->make($jwtCode);
        if ($this->imgUpload) {
            return $this->respondData($param);
        }
        return $this->respondWithData($param);
    }

    /**
     * メールを送信
     */
    protected function mailSend($mailaddress, $param, $file) {
        $settings = $this->c->get('settings');
        $from = $from = $settings['mailaddress']['from'];
        $mail = new Mail($settings);
        $path = $settings['template']['mail'];
        $fileData = $path.$file;
        $mailData = $mail->splitTitleBody($fileData);
        $mailData['body'] = $mail->replaceBody($mailData['body'], $param);
        $result = 1;
        if (!defined('IS_PHP_UNIT') && $mailaddress != '') {
            $result = $mail->mailSend($mailaddress, $from, $mailData['title'], $mailData['body']);
        }
        return $result;
    }

        /**
     * トランザクション開始
     */
    public function beginTransaction() {
        if (!\ORM::get_db()->inTransaction()) {
            \ORM::get_db()->beginTransaction();
        }
    }

    /**
     * トランザクションコミット
     */
    public function commit() {
        if (!defined('IS_PHP_UNIT')) {
            \ORM::get_db()->commit();
        }
    }

    /**
     * トランザクションロールバック
     */
    public function rollback() {
        if (\ORM::get_db()->inTransaction()) {
            \ORM::get_db()->rollBack();
        }
        if (defined('IS_PHP_UNIT')) {
            $this->beginTransaction();
        }
    }

    public function checkAdmin() {
        if (property_exists($this->session, 'admin_id')) {
            return true;
        }
        return false;
    }

    public function checkPasword($pass) {
        if (!preg_match("/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])[a-zA-Z0-9]{8,}/", $pass)) {
            return ['password' => ['大文字、小文字、数字を含めてください']];
        }
        return [];
    }
}
