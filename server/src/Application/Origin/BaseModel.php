<?php
namespace App\Application\Origin;

class BaseModel extends \Model {
    public $container = null;
    public $skipDate = false;   // 日付更新が不要ならtrueを設定する

    /**
     * コンテナ設定
     */
    public function setContainer($container) {
        $this->container = $container;
    }

    /**
     * 登録時に登録日時・更新日時を書き込む。
     */
    public function save() {
        if (!$this->skipDate) {
            $date = new \DateTime();
            if (!$this->id) {
                $this->created = $date->format('Y/m/d H:i:s');
            }
            $this->modified = $date->format('Y/m/d H:i:s');
        }
        parent::save();
    }

    /**
     * 論理削除
     */
    public function logicDelete() {
        $this->status = 9;
        parent::save();
    }

    /**
     * Validationなしで登録する。
     * 基本的にテスト用途以外の利用は不可
     */
    public function saveForce() {
        if (defined('IS_PHP_UNIT')) {
            parent::save();
        }
    }

    /**
     * 郵便番号をハイフン区切り形式に変換する。
     */
    public function formatZipCode($value) {
        if ($value == '' || strlen($value) != 7) {
            return $value;
        }
        return substr($value, 0, 3).'-'.substr($value, 3);
    }

    public function nowDate() {
        if ($this->container['settings']['now_date']) {
            return new \DateTime($this->container['settings']['now_date']);
        }
        return new \DateTime();
    }

    /**
     * 日付文字列をフォーマットする
     */
    protected function formatDate($val, $format="Y/m/d") {
        if (!$val) {
            return '';
        }
        return (new \DateTime($val))->format($format);
    }
}