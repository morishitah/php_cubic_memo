<?php
namespace App\Application\Origin;

use App\Application\Dependence\JWTInterface;
use Exception;
use DateTimeImmutable;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Validation\Constraint\IssuedBy;
use Lcobucci\JWT\Validation\Constraint\ValidAt;
use App\Exception\JwtException;

class JWT {
    var $container;
    private $config;

    public function __construct($container)
    {
        $this->container = $container;
        $setting = $this->container->get('JWT');
        $this->config = Configuration::forSymmetricSigner(new Sha256(), InMemory::plainText($setting['token']));
    }

    /**
     * @throws JwtException
     */
    public function check($request) {
        $token = $request->getHeader('token');
        $uid = "";
        if (count($token) > 0) {
            $token = $token[0];
            if ($token != '') {
                try {
                    $token = $this->config->parser()->parse((string)$token);
                } catch (Exception) {
                    throw new \Exception('署名エラーです。');
                }
                $now = new DateTimeImmutable();
                if ($token->isExpired($now)) {
                    throw new \Exception('有効期限を確認してください。');
                }
                //制約を設定
                $setting = $this->container->get('JWT');
                $this->config->setValidationConstraints(new SignedWith($this->config->signer(), $this->config->signingKey()));
                $this->config->setValidationConstraints(new IssuedBy($setting['url']));
                if (!$this->config->validator()->validate($token, ...$this->config->validationConstraints())) {
                    throw new \Exception('署名エラーです。');
                }
                $uid = $token->claims()->get('uid');
            }
        }
        return $uid;
    }

    public function make($code) {
        $setting = $this->container->get('JWT');
        $now = new DateTimeImmutable();
        $token =$this->config->builder()
            ->issuedBy($setting['url'])
            ->identifiedBy(uniqid(), true)
            ->issuedAt($now)
            ->canOnlyBeUsedAfter($now->modify('+'.$setting['after_time'].' second'))
            ->expiresAt($now->modify('+'.$setting['expire_time'].' second'))
            ->withClaim('uid', $code.'')
            ->getToken($this->config->signer(), $this->config->signingKey());
        return $token->toString();
    }
}