<?php
namespace App\Application\Origin;

class ExceptionValidate extends \Exception {
    protected $validateMessage = [];

    public function __construct($validateMessage) {
        $this->validateMessage = $validateMessage;
        parent::__construct("", 0, null);
    }

    public function getErrorMessages() {
        return $this->validateMessage;
    }

    public function getErrorMessageString() {
        $message = '';
        if (!is_array($this->validateMessage)) {
            return $this->validateMessage;
        }
        foreach ($this->validateMessage as $k => $v) {
            foreach ($v as $m) {
                $message = $k.':'.$m;
            }
        }
        return $message;
    }
}