<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Application\Dependence\DataListInterface;
use App\Application\Origin\BaseAction;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;

class ListAction extends BaseAction
{
  public function __construct(LoggerInterface $logger, ContainerInterface $c, DataListInterface $data)
  {
    parent::__construct($logger, $c);
    $data->init('Users', 'users.id');
    $this->component = $data;
  }

  protected function action(): Response {
    return $this->r(['result' => $this->component->execute()]);
  }
}