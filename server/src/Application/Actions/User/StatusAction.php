<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Application\Origin\BaseAction;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use \App\Application\Component\Login\Login;

class StatusAction extends BaseAction
{
  public function __construct(LoggerInterface $logger, ContainerInterface $c)
  {
    parent::__construct($logger, $c);
  }
  
  protected function action(): Response {
    $rtnData = [
      'id' => $_SESSION['id'],
      'name' => $_SESSION['name'],
    ];
    return $this->r(['result' => $rtnData]);
  }
}