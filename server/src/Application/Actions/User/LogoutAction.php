<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Application\Origin\BaseAction;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;

class LogoutAction extends BaseAction
{
  public function __construct(LoggerInterface $logger, ContainerInterface $c)
  {
    parent::__construct($logger, $c);
  }

  protected function action(): Response {
    $this->clearSession();
    return $this->r([]);
  }
}