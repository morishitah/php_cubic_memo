<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Application\Dependence\DataSaveDependence;
use App\Application\Origin\BaseAction;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;

class SaveAction extends BaseAction
{
  public function __construct(LoggerInterface $logger, ContainerInterface $c, DataSaveDependence $component)
  {
    parent::__construct($logger, $c);
    $component->init('Users');
    $this->component = $component;
  }

  protected function action(): Response {
    $this->component->setContainer($this->c);
    $this->component->setSetterCallback(function($query, $param) {
      $query->setContainer($this->c);
      if ($query->id) {
        unset($query->password);
        $query->checkPassword = false;
      }

      return $query;
    });
    $errors = $this->component->execute();
    return $this->r($errors);
  }
}