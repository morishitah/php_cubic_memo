<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Application\Origin\BaseAction;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use \App\Application\Origin\ExceptionValidate;

class DeleteAction extends BaseAction
{
  public function __construct(LoggerInterface $logger, ContainerInterface $c)
  {
    parent::__construct($logger, $c);
  }

  protected function action(): Response {
    try {
      $model = \Model::factory('Users');
      $query = $model->find_one($this->p->id);
      if (!$query) {
        return $this->r(['errors' => []]);
      }

      $query->checkPassword = false;
      $query->status = 9;
      $query->save();
    } catch (ExceptionValidate $e) {
      return $this->r(['errors' => $e->getErrorMessages()]);
    }

    return $this->r(['errors' => []]);
  }
}