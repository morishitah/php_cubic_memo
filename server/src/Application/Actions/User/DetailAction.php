<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Application\Dependence\DataDetailDependence;
use App\Application\Origin\BaseAction;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;

class DetailAction extends BaseAction
{
  public function __construct(LoggerInterface $logger, ContainerInterface $c, DataDetailDependence $component)
  {
    parent::__construct($logger, $c);
    $component->init('Users', [
      'id' => '',
      'loginid' => '',
      'name' => '',
    ]);
    $this->component = $component;
  }

  protected function action(): Response {
    return $this->r(['result' => $this->component->execute()]);
  }
}