<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Application\Origin\BaseAction;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use App\Application\Component\Cookie;

class LoginCookieAction extends BaseAction
{
  public $checkLogin = false;

  public function __construct(LoggerInterface $logger, ContainerInterface $c)
  {
    parent::__construct($logger, $c);
  }

  protected function action(): Response {
    $cookie = new Cookie($this->request);
    $result = $cookie->get($this->c->get('settings')['cookie']['user']);
    return $this->r(['test' => $result]);
  }
}