<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Application\Origin\BaseAction;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use \App\Application\Component\Login\Login;

class LoginAction extends BaseAction
{
  public $checkLogin = false;

  public function __construct(LoggerInterface $logger, ContainerInterface $c)
  {
    parent::__construct($logger, $c);
  }

  protected function action(): Response {
    $result = [];
    try {
      $login = new Login('Users', $this->p, $this->request, $this->response, $this->logger);
      $result = $login->execute('loginid', $this->c->get('settings')['cookie']['user'], function($row) {
        $this->writeSession('id', $row->id);
        $this->writeSession('name', $row->name);
      });
      $this->response = $login->getResponse();
    } catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      $result['error'] = [$e->getMessage()];
    }
    return $this->r($result);
  }
}