<?php
declare(strict_types=1);

namespace App\Application\Actions\Memo;

use App\Application\Origin\BaseAction;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use \App\Application\Origin\ExceptionValidate;

class DeleteAction extends BaseAction
{

  public function __construct(LoggerInterface $logger, ContainerInterface $c)
  {
    parent::__construct($logger, $c);
  }

  protected function action(): Response {

    $model = \Model::factory('Memos');
    $query = $model->find_one($this->p->id);
    if (!$query) {
      return $this->r(['errors' => []]);
    }

    $query->status = 9;
    $query->save();

    return $this->r(['errors' => []]);
  }
}