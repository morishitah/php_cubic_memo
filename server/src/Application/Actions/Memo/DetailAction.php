<?php
declare(strict_types=1);

namespace App\Application\Actions\Memo;

use App\Application\Dependence\DataDetailDependence;
use App\Application\Origin\BaseAction;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;

class DetailAction extends BaseAction
{
  public function __construct(LoggerInterface $logger, ContainerInterface $c, DataDetailDependence $component)
  {
    parent::__construct($logger, $c);
    $component->init('Memos', [
      'id' => '',
      'user_id' => '',
      'title' => '',
      'contents' => '',
      'share_flg' => '',
      'tags' => [
        'mode' => 'child',
        'model' => 'MemoTags',
        'key' => 'name',
        'child_field' => 'memo_id',
        'field' => [
          'name' => '',
      ],
    ]]
  );
    $this->component = $component;
  }

  protected function action(): Response {
    return $this->r(['result' => $this->component->execute()]);
  }
}