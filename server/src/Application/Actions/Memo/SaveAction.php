<?php
declare(strict_types=1);

namespace App\Application\Actions\Memo;

use App\Application\Dependence\DataSaveDependence;
use App\Application\Origin\BaseAction;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use \App\Application\Origin\ExceptionValidate;

class SaveAction extends BaseAction
{
  public function __construct(LoggerInterface $logger, ContainerInterface $c, DataSaveDependence $component)
  {
    parent::__construct($logger, $c);
    $component->init('Memos');
    $this->component = $component;
  }
  
  protected function action(): Response {
    $this->p->user_id = $_SESSION['id'];
    $tags = $this->p->tags;
    unset($this->p->tags);
    $id = \Model::factory('Memos')->max('id') + 1;
    $model = \Model::factory('MemoTags');
    if (property_exists($this->p, 'id')) {
      $id = $this->p->id;
      $rows = $model->where('memo_id', $this->p->id)->find_many();
      foreach ($rows as $row) {
        $row->delete();
      }
    }
    foreach ($tags as $tag) {
      $model = \Model::factory('MemoTags');
      $query = $model->create();
      $query->memo_id = $id;
      $query->name = $tag;
      try {
        $query->save();
      } catch (ExceptionValidate $e) {
          $errors = $e->getErrorMessages();
      }
    }
    $errors = $this->component->execute();
    return $this->r($errors);
  }
}