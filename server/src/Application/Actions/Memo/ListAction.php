<?php
declare(strict_types=1);

namespace App\Application\Actions\Memo;

use App\Application\Dependence\DataListInterface;
use App\Application\Origin\BaseAction;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;

class ListAction extends BaseAction
{
  public function __construct(LoggerInterface $logger, ContainerInterface $c, DataListInterface $data)
  {
    parent::__construct($logger, $c);
    $data->init('Memos', 'memos.id DESC');
    $this->component = $data;
  }

  protected function action(): Response {
    $this->component->setQueryCallback($this->p, function($param, $query) {
      $shareFlg = '1';
      if ($param) {
        if (property_exists($param, 'title') && $param->title) {
          $query->where_like('title', $this->likeString($param->title));
        }
        if (property_exists($param, 'share_flg')) {
          $shareFlg = $this->p->share_flg;
        }
        if (property_exists($param, 'user_id') && $param->user_id) {
          $query->where('user_id', $param->user_id);
        }
        if (property_exists($param, 'tag') && $param->tag) {
          $query->where_raw("`id` in (SELECT `memo_id` FROM memo_tags WHERE `name` LIKE ?  GROUP BY `memo_id`)", [$this->likeString($param->tag)]);
        }
      } else {
        $query->where_raw("(`share_flg` = '1' OR `user_id` = " . $_SESSION['id'] .")");
      }

      $whereLogic = 'OR';
      if ($shareFlg === '0') {
        $whereLogic = 'AND';
      }
      $query->where_raw("(`share_flg` = ? ".$whereLogic." `user_id` = ?)", [$shareFlg, $_SESSION['id']]);

      return $query;
    });
    $this->component->setResultCallback(function($rows){
      $result = [];
      foreach ($rows as $val) {
        $tagList = [];
        $model = \Model::factory('MemoTags');
        $tags = $model->where('memo_id', $val->id)->where_not_equal('status', 9)->find_many();
        foreach ($tags as $tag) {
          $tagList[] = $tag->name;
        }
        $result[] = [
          'id' => $val->id,
          'user_id' => $val->user_id,
          'name' => $val->name,
          'title' => $val->title,
          'share_flg' => $val->share_flg,
          'tags' => $tagList,
        ];
      }
      return $result;
    });
    return $this->r(['result' => $this->component->execute()]);
  }
}