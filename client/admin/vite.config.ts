import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import {fileURLToPath, URL} from 'url'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  server: {
    port: 3000,
    // open: true,
    proxy: {
      "/_api/": {
        target: 'http://192.168.33.254',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/_api/, '/_api/')
      },
    },
  },
  resolve: {
    alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
    }
  },
  css: {
    modules: {
      scopeBehaviour: 'local',
    },
    preprocessorOptions: {
      sass: {
        additionalData: `@import "./src/sass/const.sass"`,
      },
    },
  },
})
