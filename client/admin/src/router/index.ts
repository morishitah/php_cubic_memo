import { createRouter, createWebHistory } from 'vue-router'

//fix:APIを接続したらtrueにする
const requiredAuth = true

const scrollBehavior = (to: any, from:any, savedPosition: any) => {
  if (savedPosition) {
    return savedPosition;
  } else {
    return { top: 0 }
  }
};

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: () => import('@/views/login.vue'),
    },
    {
      path: '/memo',
      component: () => import('@/views/mainView.vue'),
      children: [
        {
          path: '',
          component: () => import('@/views/memo/list.vue')
        },
        {
          path: 'edit/:id?',
          component: () => import('@/views/memo/edit.vue')
        }
      ]
    },
    {
      path: '/member',
      component: () => import('@/views/mainView.vue'),
      children: [
        {
          path: '',
          component: () => import('@/views/member/list.vue')
        },
        {
          path: 'edit/:id?',
          component: () => import('@/views/member/edit.vue')
        }
      ]
    },

    //1番下に書く
    {
      path: '/:catchAll(.*)',
      name: '404',
      component: () => import('../views/404View.vue'),
    },
  ],
  scrollBehavior
})





// ナビゲーションガード
// router.beforeEach((to, from, next) => {
//   if (to.matched.some(r => r.meta.requiredAuth)) {
//     // 認証チェック
//     if (useCookies().cookies.get('__token')) {
//         next();
//     } else {
//         next({name: 'login'});
//     }
//   } else {
//     next();
//   }
// });

export default router