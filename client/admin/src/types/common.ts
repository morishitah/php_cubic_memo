export interface ResponseData {
  state: number;
  errors: any;
  result: any;
  detail?: any;
  list?: any;
  pages?: any;
}