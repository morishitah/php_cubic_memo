import { createApp } from 'vue'
import './style.css'
import router from './router'
import { createPinia } from "pinia"
import App from './App.vue'

const app = createApp(App)
	.use(createPinia())
	.use(router)
	.mount('#app')
