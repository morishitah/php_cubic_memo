import axios from "axios";
import type {AxiosInstance} from "axios";
import router from "@/router";
import type {ResponseData} from "@/types/common";
import {useTest} from '@/store/store'
import {storeToRefs} from 'pinia'

export class ApiClient {
  private _axiosBase: AxiosInstance
  private _resultParse(res: any) {
    if (res.data.data.state == 444 || res.data.data.state == 500) {
      router.push('/')
      return false
    }
    return true
  }

  constructor() {
    this._axiosBase = axios.create({
      baseURL: import.meta.env.VITE_API_BASE_URL,
      headers: {
        'Content-Type': 'application/json',
      }
    })

    //リクエストの共通処理
    this._axiosBase.interceptors.request.use(config => {
      return config;
    });

    //レスポンスの共通処理
    this._axiosBase.interceptors.response.use(res => {
      if (!this._resultParse(res)) {
        return
      }
      return res;
    });
  }

  public get(endPoint: string): Promise<ResponseData> {
    const store = useTest()
    store.setErrors({})

    const result = this._axiosBase.get(
      endPoint
    ).then(
      function(res) {
        return res.data.data
      }
    ).catch(
      function(err) {
        store.setErrors(err.response.data.errors)
        return {
          state: err.response.status
        }
      }
    )
    return result
  }

  public post(endPoint: string, param: object): Promise<ResponseData> {
    const json = JSON.stringify(param)
    // const params = new URLSearchParams()
    // params.append('data', json)
    const store = useTest()
    store.setErrors({})

    const result = this._axiosBase.post(
      endPoint,
      json,
      ).then(
        function(res) {
          store.setErrors(res.data.data.errors)
          return {
            state: res.data.data.state,
            errors: res.data.data.errors,
            result: res.data
          }
        }
      ).catch(
        function(err) {
          store.setErrors(err.response.data.errors)
          return {
            state: err.response.status,
            errors: [],
            result: []
          }
        }
      )
    return result
  }
}