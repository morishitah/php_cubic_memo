export class Global {
  public status() {
    const d = [
      {id: 1, name: '承認待ち'},
      {id: 2, name: '提出済み'},
      {id: 3, name: '発注確定'},
      {id: 4, name: '請求待ち'},
      {id: 5, name: '請求済み'},
      {id: 99, name: 'キャンセル'},
    ]
    return d
  }
}