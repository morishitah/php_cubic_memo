import { defineStore } from "pinia";
import {ApiClient} from "@/lib/http"
import type {ResponseData} from "@/types/common";

//defineStoreの第1引数はユニークな値を設定
export const useTest = defineStore('store', {
  state: () => {
    return {
      text: '',
      token: null,
      errors: {},
      loginState: null as Promise<ResponseData>,
      memoSearch: null,
    }
  },
  getters: {
    getText(state) {
      return state.text;
    },
    getToken(state) {
      return state.token;
    },
    getErrors(state) {
      return state.errors;
    },
    getMemoSearch(state) {
      return state.memoSearch;
    },
    async getLoginState(state) {
      if (!state.loginState) {
        await this.setLoginState()
      }
      return state.loginState;
    },
  },
  //mutationsがないので、stateの更新はactionsで行う
  actions: {
    setText(text: string) {
      this.text = text;
    },
    setErrors(errors) {
      if (!errors) {
        errors = {};
      }
      this.errors = errors;
    },
    setMemoSearch(param) {
      this.memoSearch = param;
    },
    async setLoginState() {
      const api = new ApiClient();
      const result = await api.get('/login/state');
      this.loginState = {'detail': result.result};
    },
  },
})