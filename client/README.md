# Nodeの切り替えに使うコマンド
- nvm
# 今までの開発で使ってるバージョン
- v14.17.6
# バージョン切り替え
#### 管理者権限がいるのでPowerShellで行う
- nvm install 19.3.0
- nvm use 19.3.0
- nvm use 14.17.6
- npm install -g yarn

# puppeteer インストール
npm install puppeteer

# Vite作成
yarn create vite
